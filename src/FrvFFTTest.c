/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------*/
/* test the FFT code */
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "FrvFFT.h"
  
double UMrgausd(long i);

/*--------------------------------------------------------------------*/
int main(int argc, char *argv[])
/*--------------------------------------------------------------------*/
{struct FrVect *vect;
 struct FrvRFFT *fft;
 double *output,dt, phase;
 int nData, i, iVect, io, nprint;
 char *option[] = {"","H","HSA","A","HN","SP","HSP","AHNSP","END"};

 phase = 0;
 nData = 1000;
 dt = 1./nData;
 vect = FrVectNew1D("test vector",FR_VECT_8R, nData,dt,"","");

 for(i=0; i<nData; i++)
   {vect->dataD[i] = UMrgausd(0) + 0.*sin(phase);
    phase += 3.141592654*10/nData;}

     /*------------ first test the fft without resampling ------*/

 for(io=0; strcmp(option[io],"END") != 0; io++)
   {printf(" test option %d %s\n",io, option[io]);

    if(io == 0)
         fft = FrvRFFTNew(NULL,0,0);
    else fft = FrvRFFTNew(option[io],0,0);
    for(iVect=0; iVect<100; iVect++)
      {for(i=0; i<nData; i++)
         {vect->dataD[i] = UMrgausd(0) + 1.*sin(phase);
         phase += 3.1416*10/nData;}
       fft = FrvRFFTFor(fft, vect);}

    if(fft->amplitude == NULL) continue;

    output = fft->amplitude->dataD;

    nprint = fft->amplitude->nData;
    if(nprint > 20) nprint = 20;
   
    printf("  i   input   real part  imaginary part  amplitude   <amplitude>\n"); 
    for(i=0; i<nprint; i++)
      {printf("%3d %7.2f %12.5e %12.5e ", i, vect->dataD[i],
	      output[2*i],output[2*i+1]);
       if(fft->amplitude != NULL) printf("%12.6f", fft->amplitude->dataD[i]);
       if(fft->amplitudeA!= NULL) printf("%12.6f", fft->amplitudeA->dataD[i]);
       printf("\n");}


    printf(" a second call to check if it's the same result\n");
    fft = FrvRFFTFor(fft, vect);

    for(i=0; i<nprint; i++)
      {printf("%3d %7.2f %12.5e %12.5e ", i, vect->dataD[i],
	      output[2*i],output[2*i+1]);
       if(fft->amplitude != NULL) printf("%12.6f", fft->amplitude->dataD[i]);
       if(fft->amplitudeA!= NULL) printf("%12.6f", fft->amplitudeA->dataD[i]);
       printf("\n");}
    
   }

 /*------- now some resampling test -----*/

  printf(" now FrvRFFTNew(\"AP\", 2*nData, 0);\n");
  fft = FrvRFFTNew("AP", 2*nData, 0);
  for(iVect=0; iVect<100; iVect++)
     {for(i=0; i<nData; i++)
        {vect->dataD[i] = UMrgausd(0) + sin(phase);
         phase += 3.141592654*10/nData;}
       fft = FrvRFFTFor(fft, vect);}

  output = fft->amplitude->dataD;

  nprint = fft->amplitude->nData;
  if(nprint > 20) nprint = 20;
   
  printf("  i   input   real part  imaginary part  amplitude \n"); 
  for(i=0; i<nprint; i++)
      {printf("%3d %7.2f %12.5e %12.5e ", i, vect->dataD[i],
	      output[2*i],output[2*i+1]);
       if(fft->amplitude != NULL) printf("%12.6f", fft->amplitude->dataD[i]);
       if(fft->amplitudeA!= NULL) printf("%12.6f", fft->amplitudeA->dataD[i]);
       printf("\n");}

  printf(" now FrvRFFTNew(\"AP\", 1.5*nData, 2);\n");
  fft = FrvRFFTNew("AP", 1.5*nData, 2);
  for(iVect=0; iVect<100; iVect++)
     {for(i=0; i<nData; i++)
        {vect->dataD[i] = UMrgausd(0) + sin(phase);
         phase += 3.141592654*10/nData;}
       fft = FrvRFFTFor(fft, vect);}

  output = fft->amplitude->dataD;

  nprint = fft->amplitude->nData;
  if(nprint > 20) nprint = 20;
   
  printf("  i   input   real part  imaginary part  amplitude \n"); 
  for(i=0; i<nprint; i++)
      {printf("%3d %7.2f %12.5e %12.5e ", i, vect->dataD[i],
	      output[2*i],output[2*i+1]);
       if(fft->amplitude != NULL) printf("%12.6f", fft->amplitude->dataD[i]);
       if(fft->amplitudeA!= NULL) printf("%12.6f", fft->amplitudeA->dataD[i]);
       printf("\n");}

  printf(" now FrvRFFTNew(\"HAP\", nData/2, 0);\n");
  fft = FrvRFFTNew("HAP", nData/2, 0);
  for(iVect=0; iVect<100; iVect++)
     {for(i=0; i<nData; i++)
        {vect->dataD[i] = UMrgausd(0) + sin(phase);
         phase += 3.141592654*10/nData;}
       fft = FrvRFFTFor(fft, vect);}

  output = fft->amplitude->dataD;

  nprint = fft->amplitude->nData;
  if(nprint > 20) nprint = 20;
   
  printf("  i   input   real part  imaginary part  amplitude \n"); 
  for(i=0; i<nprint; i++)
      {printf("%3d %7.2f %12.5e %12.5e ", i, vect->dataD[i],
	      output[2*i],output[2*i+1]);
       if(fft->amplitude != NULL) printf("%12.6f", fft->amplitude->dataD[i]);
       if(fft->amplitudeA!= NULL) printf("%12.6f", fft->amplitudeA->dataD[i]);
       printf("\n");}

  printf(" FFTW word size was:%d\n",fftw_sizeof_fftw_real());
  return(0);
}

/*----------------------------------------------------- UMrgausd -----*/
/* rangausd generates a pseudo random number (double) with normal     */
/* (Gaussian) distribution with zero mean and unit variance.          */
/* Author: B. Mours        27/02/91                                   */
/* Correction by: X. Grave 13/04/95                                   */
/*--------------------------------------------------------------------*/
double UMrgausd(long i)
{
 static long iset=0;
 static double gset;
 double fac,r,x,y;
 long a = 16807;
 long m = 2147483647;
 long q = 127773;
 long p = 2836;
 static long seed = 2147483646;
 long hi, lo;

 if (i != 0)  seed = i;
 
 if (iset == 1 )
    {iset = 0;
     return(gset);
    }
 
 iset = 1;
 do {
     hi   = seed / q;
     lo   = seed - hi * q;
     seed = a*lo - p*hi;
     if (seed <= 0) seed = seed + m;
     x    = -1. + 2. * (double)seed / (double)m;
     hi   = seed / q;
     lo   = seed - hi * q;
     seed = a*lo - p*hi;
     if (seed <= 0) seed = seed + m;
     y    = -1. + 2. * (double)seed / (double)m;
     r    = x*x + y*y;
    }
 while (r >= 1.0);
 
 if (r == 0.0)  r = 0.000001;
 fac  = sqrt(-2.*log(r)/r);
 gset = x * fac;

 return(y*fac);
}
