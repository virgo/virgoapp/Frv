/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*--------------  FrvBuf.h by B.Mours Feb 21, 2005--------------------------*/

#ifndef FRVBUF
#define FRVBUF

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include "FrameL.h"
#include "FrvCopy.h"

#ifdef __cplusplus
extern "C" {
#endif

struct FrvBuf {              /*  This is the FrVect Buffering object         */
                             /*--------------------- input data -------------*/
  int nDataOut;              /* output vector size (number of bin)           */
  int step;                  /* step size (number of output bins)            */
  int outType;               /* type for the output vector(-1 means as input)*/
  int decimate;              /* decimation                                   */
  int decSign;               /* decim. sign used by FrVectDecimate (-1 or +1)*/
  int delay;                 /* internal buffer                              */
  FRBOOL intDelay;           /* if YES, the data are just shifted            */
  int gpsOffset;             // if >= 0, output is GPS aligned with this offset
                             /*------------------- internal data ------------*/
  char* work;                /* working area                                 */
  long first;                /* index of the first point in the work space   */
  long last;                 /* index of the last point in the work space    */
  long workSize;             /* # of elements (not bytes) for the work space*/
  double lastGTime;
  FrVect *input;             /* input working vector (point to work)         */
                             /* ------------output-------------------------  */
  FrVect *output;            /* output data  (point to work)                 */
};

typedef struct FrvBuf FrvBuf;

void    FrvBufFree   (FrvBuf* buffer);
int     FrvBufFeed   (FrvBuf *buffer, FrVect *vect);
int     FrvBufFeedN  (FrvBuf *buffer, FrVect *vect, int iStart, int nData);
int     FrvBufGetNext(FrvBuf *buffer);
int     FrvBufInit   (FrvBuf *buffer, FrVect *vect); 
FrvBuf* FrvBufNew  (int outSize,int step,int outType,int decimate,int delay);
FrvBuf* FrvBufNewID(int outSize,int step,int outType,int decimate,int delay);
void    FrvBufSetAligned(FrvBuf *buffer, int offset);
int     FrvBufStillToGo(FrvBuf *buffer);

#ifdef __cplusplus
}
#endif

#endif



