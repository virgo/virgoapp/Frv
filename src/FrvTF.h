/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*  FrvTF.h by  F. Marion and B. Mours   Mar 01, 2005           */

#ifndef FRVTF
#define FRVTF

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include "FrameL.h"
#include "FrvBuf.h"
#include "FrvFFT.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FrvCoGap FrvCoGap;
typedef struct FrvTF FrvTF;

struct FrvCoGap {
  FrvTF *tf;                /* transfert function object                     */
  char *channel1;           /* first channel                                 */
  char *channel2;           /* second channel                                */
  int nAverage;             /* number of FFT averaged                        */
  double freqMin;           /* frequency start for the band                  */
  double freqMax;           /* band frequency stop;                          */
  double threshold;         /* threshold used to compute the coherence gap   */
  double maxFreqGap;        /* largest frequency gap of data below threshold */
  double meanCoherence;     /* mean coherence in the frequency band          */
  double startGap;          /* starting frequency of the maximum gap         */
  double GTime;             /* GPS time for the last produced result         */
};

void      FrvCoGapFree(FrvCoGap *coGap);
FrvCoGap* FrvCoGapNew(char *channel1,  char *channel2,
                      double FFTduration, int nAverage,
                      double freqMin, double freqMax, double threshold);
int FrvCoGapProc(FrvCoGap* coGap, FrameH *frame);


struct FrvTF {
                             /*--------------------- input data -------------*/
  double decay;              /* decay time for averageing (could by changed) */
  long nAverage;             /* #of fft for the mean spectrum/correaltion    */
  int  outSize;              /* number of bin for the transfert function     */
  int decimate;              /* decimation                                   */
  double duration;           /* time length used to compute the FFT          */
                             /*------------------- internal data ------------*/
  FRBOOL  optionC;           /* if = FR_YES compute the coherence            */
  FRBOOL  optionE;           /* if = FR_YES compute the TF errors            */
  FrVect *output;            /* transfer function (last)                     */
  FrVect *correlation;       /* mean correaltion in the frequency domain     */
  FrVect *modulus;           /* transfer function: module (mean)             */
  FrVect *phase;             /* transfer function: phase (mean)              */
  FrVect *coherence;         /* transfer function: coherence (if optionC=YES)*/
  FrVect *errorM;            /* error on the module (if optionE=YES)         */
  FrVect *errorP;            /* error on the phase  (if optionE=YES)         */
  FrvBuf *bufferS;           /* buffer for the signal                        */
  FrvBuf *bufferR;           /* buffer for the reference                     */
  FrvRFFT *fftS;             /* fft for the signal                           */
  FrvRFFT *fftR;             /* fft for the reference                        */
  long nCall;                /* number of calls                              */
  char err[256];             /* hold error message if any                    */
};


void   FrvTFError(FrvTF *tf);
void   FrvTFFree(FrvTF *tf);
int    FrvTFInit(FrvTF *tf, FrVect *signal, FrVect *ref);
FrvTF* FrvTFNew(char* option, int outsize, int decimate);
FrvTF* FrvTFNewT(char* option, double duration, int nAverage);
int    FrvTFProc(FrvTF *tf, FrVect *signal, FrVect *ref);
void   FrvTFReset(FrvTF* tf);

#ifdef __cplusplus
}
#endif
#endif
