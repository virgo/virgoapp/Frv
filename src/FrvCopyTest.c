/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------*/
/* test the FrvBasic code */
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "FrvCopy.h"
  
/*---------------------------------------------------------------------------*/
void testCopy(FrVect *vect)
{FrVect *out;

 printf("\n\n--- Test FrCopy with the following vector ----------\n");
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrVectCopy ---------------------------\n");
 out = FrVectCopy(vect);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrVectCopy vect = NULL ---------------------------\n");
 out = FrVectCopy(NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvClone ---------------------------\n");
 out = FrvClone(vect,"clone");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyTo with scale = 2. ---------\n");
 out = FrvCopyTo(vect, 2., vect);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyTo with scale = 1. ---------\n");
 out = FrvCopyTo(vect, 1., vect);
 FrVectDump(out, stdout, 2);
 
 printf("--- Test FrvCopyTo with copy = NULL. ---------\n");
 out = FrvCopyTo(vect, 1., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyTo with vect = NULL. ---------\n");
 out = FrvCopyTo(NULL, 1., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToF with scale = 2, newName = NULL ---------\n");
 out = FrvCopyToF(vect, 2., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToF with scale = 1 ---------\n");
 out = FrvCopyToF(vect, 1., "scale = 1");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToD with scale = 2, newName = NULL ---------\n");
 out = FrvCopyToD(vect, 2., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToD with scale = 1 ---------\n");
 out = FrvCopyToD(vect, 1., "scale = 1");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToI with scale = 2, newName = NULL ---------\n");
 out = FrvCopyToI(vect, 2., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToI with scale = 1 ---------\n");
 out = FrvCopyToI(vect, 1., "scale = 1");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToS with scale = 2, newName = NULL ---------\n");
 out = FrvCopyToS(vect, 2., NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvCopyToS with scale = 1 ---------\n");
 out = FrvCopyToS(vect, 1., "scale = 1");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimateF with nGroup = 4 ---------\n");
 out = FrvDecimateF(vect, 4, "nGroup = 4");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimateF with nGroup = 2, newName = NULL ---------\n");
 out = FrvDecimateF(vect, 2, NULL);
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimateS with nGroup = 2 ---------\n");
 out = FrvDecimateS(vect, 2, "nGroup = 2");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimateI with nGroup = 2 ---------\n");
 out = FrvDecimateI(vect, 2, "nGroup = 2");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimateD with nGroup = 2 ---------\n");
 out = FrvDecimateD(vect, 2, "nGroup = 2");
 FrVectDump(out, stdout, 2);

 printf("--- Test FrvDecimate with nGroup = 2 ---------\n");
 out = FrvDecimate(vect, 2, NULL);
 FrVectDump(out, stdout, 2);

 return;
}
/*---------------------------------------------------------------------------*/
int main(int argc, char *argv[])   /*-------------- main ----*/
{struct FrVect *vect;
 int nData, i;

 nData = 512;

 vect = FrVectNew1D("original",FR_VECT_8R, nData,1.,"","");

 for(i=0; i<nData; i++)
   {vect->dataD[i] = 1.1*i;}

 testCopy(vect);
 
 vect = FrvCopyToS(vect, 1., "new");

 testCopy(vect);

 return(0);
}
