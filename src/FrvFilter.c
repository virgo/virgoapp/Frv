/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*------- FrvFilter.h   -----------------------------------------------------*/

#include "FrvFilter.h"
#include <string.h>
#include <math.h>

/*------------------------------------------------------- FrvFilterButFree --*/
void  FrvFilterButFree(FrvFilterB* filter)
/*---------------------------------------------------------------------------*/
{
  if(filter == NULL) return;

  if(filter->next     != NULL) FrvFilterButFree(filter->next);
  if(filter->name     != NULL) free(filter->name);
  if(filter->filterHP != NULL) FrvFilter2Free(filter->filterHP);
  if(filter->filterLP != NULL) FrvFilter2Free(filter->filterLP);

  free(filter);

  return;
}
/*---------------------------------------------------------------------------*/
FrvFilterB* FrvFilterButNew(int order, double fMin, double fMax, char* name)
/*---------------------------------------------------------------------------*/
{
  FrvFilterB *filter;

  filter = (FrvFilterB*) calloc(1, sizeof(FrvFilterB));
  if(filter == NULL) return(NULL);
  if(name != NULL) FrStrCpy(&(filter->name),name);
  filter->fMin    = fMin;
  filter->fMax    = fMax;
  filter->order   = order;
  filter->filterHP= NULL;
  filter->filterLP= NULL;
  filter->next    = NULL;
  filter->errorPrinted = FR_NO;
  filter->lastError    = 0; 

  return(filter);
}
/*---------------------------------------------------------------------------*/
FrvFilter2* FrvFilterButIni(double a12, double a0, double fCut, 
                            double freqData, int order)
/*---------------------------------------------------------------------------*/
{
  FrvFilter2 *filter2, *root;
  double a1, a2, b1, b2, bMax, omega, fLimit, f;
  int i;

  root = NULL;

  /*--------correct for the bilinear tranform warping of the frequency cut---*/
  fLimit = .4999999*freqData;
  if(fCut > fLimit) f = fLimit;
  else              f = fCut;
  omega = 2*freqData*tan(FRTWOPI*f/(2*freqData));

  a1 = a12/omega;
  a2 = a12/omega/omega;

  for(i = 1; i<=order/2; i++) {
    bMax = -2*cos((FRPI*(2*i+order-1))/(2.*order));
    if(omega > 0) {
      b2 = 1./(omega*omega);
      b1 = bMax/omega;}
    else {
      b2 = 0;
      b1 = 0;}
    filter2 = FrvFilter2New(a2, 0, a0, b2, b1, 1.);
    if(filter2 == NULL) return(NULL);

     /*--to save space, keep only the output vector of the last added filter--*/
    if(2*i < order)   filter2->outputIsInput = FR_YES;

    filter2->next = root;
    root = filter2;}

  if(order%2 != 0) {
    filter2 = FrvFilter2New(0., a1, a0, 0.,1./omega,1.);
    if(filter2 == NULL) return(NULL);
    filter2->next = root;
    root = filter2;}

  return(root);
}
/*---------------------------------------------------------------------------*/
FrVect* FrvFilterButProc(FrvFilterB *but,
                         FrVect* vect)
/*---------------------------------------------------------------------------*/
{
  FrvFilter2 *f;
  double fData, nResets;

  if(but  == NULL)     return(NULL);
  if(vect == NULL)     return(NULL);
  if(vect->dx[0] == 0) return(NULL);

  /*-------------------------------------------------------low pass filter---*/
  if(but->fMin > 0) {
    if(but->filterHP == NULL) {
      fData = 1./vect->dx[0];
      but->filterHP = FrvFilterButIni(1., 0., but->fMin, fData, but->order);
      if(but->filterHP == NULL) return(NULL);}

    nResets = 0;
    for(f = but->filterHP; f != NULL; f = f->next) {
      FrvFilter2Proc(f,vect);
      vect = f->output;

      nResets += f->nResets;
      f->nResets = 0;}

    if(nResets != 0) {/*----reset all biquads if one of them had a problem---*/
      but->nResets++;
      for(f = but->filterHP; f != NULL; f = f->next) {
	f->reg0 = 0;
	f->reg1 = 0;
	f->reg2 = 0;}}}

  /*------------------------------------------------------high pass filter---*/
  if(but->fMax > 0) {
    if(but->filterLP == NULL) {
      fData = 1./vect->dx[0];
      but->filterLP = FrvFilterButIni(0., 1., but->fMax, fData, but->order);
      if(but->filterLP == NULL) return(NULL);
      if(but->filterHP != NULL) but->filterLP->outputIsInput = FR_YES;}

    nResets = 0;
    for(f = but->filterLP; f != NULL; f = f->next) {
      FrvFilter2Proc(f,vect);
      vect = f->output;

      nResets += f->nResets;
      f->nResets = 0;}

    if(nResets != 0) {/*----reset all biquads if one of them had a problem---*/
      but->nResets++;
      for(f = but->filterLP; f != NULL; f = f->next) {
	f->reg0 = 0;
	f->reg1 = 0;
	f->reg2 = 0;}}}

  but->output = vect;

  return(vect);
}

/*--------------------------------------------------------- FrvFilter2Free --*/
void  FrvFilter2Free(FrvFilter2* filter)
/*---------------------------------------------------------------------------*/
{
  if(filter == NULL) return;

  if(filter->next   != NULL) FrvFilter2Free(filter->next);
  if(filter->output != NULL &&
     filter->outputIsInput != FR_YES) FrVectFree(filter->output);

  free(filter);

  return;
}
/*------------------------------------------------------ FrvFilterButReset---*/
void  FrvFilterButReset(FrvFilterB* filter)
/*---------------------------------------------------------------------------*/
{
  if(filter == NULL) return;

  if(filter->filterHP != NULL) FrvFilter2Free(filter->filterHP);
  if(filter->filterLP != NULL) FrvFilter2Free(filter->filterLP);
  
  filter->filterHP = NULL;
  filter->filterLP = NULL;

  filter->errorPrinted = FR_NO;
  filter->lastError    = 0; 

  return;
}
/*---------------------------------------------------------- FrvFilter2Proc--*/
int FrvFilter2Proc(FrvFilter2 *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the direct FILTER algorithm "filter" to the vector "vect"       */
/*---------------------------------------------------------------------------*/
{double input;
 int i;

 if(vect    == NULL) return(1);
 if(filter  == NULL) return(2);  
 
 if(filter->output == NULL)
   {if(FrvFilter2Init(filter, vect) == 1) return(3);}

          /*------------------ check that we work on the same buffer length--*/

 if(filter->output->nData != vect->nData) 
   {sprintf(filter->error," nData missmatch:%ld %ld\n",
                   filter->output->nData, vect->nData);
    return(4);}

         /*-------------------- set start time for all the series defined ---*/

 filter->output->GTime  = vect->GTime;

 input = 0.;
 for(i=0; i<vect->nData; i++)
   {if     (vect->type == FR_VECT_4R) input = vect->dataF[i];
    else if(vect->type == FR_VECT_8R) input = vect->dataD[i];
    else if(vect->type == FR_VECT_2S) input = vect->dataS[i];
    else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
    else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
    else if(vect->type == FR_VECT_C)  input = vect->data[i];
    else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
    else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
    else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
    else if(vect->type == FR_VECT_1U) input = vect->dataU[i];
    filter->reg2 = filter->reg1;
    filter->reg1 = filter->reg0;
    filter->reg0 = input 
                 + filter->beta1 * filter->reg1
                 + filter->beta2 * filter->reg2; 
    filter->output->dataD[i] = filter->alfa0 * filter->reg0
                             + filter->alfa1 * filter->reg1
                             + filter->alfa2 * filter->reg2;}

 filter->nCall++;

 /*-----------------reset register is they have not normal value.
   ---------------this could come from bad input data---------------------------*/

 
 if(isfinite(filter->reg0) == 0 ||
    isfinite(filter->reg1) == 0 ||
    isfinite(filter->reg2) == 0) {
   filter->reg0 = 0;
   filter->reg1 = 0;
   filter->reg2 = 0;
   filter->nResets++;
   FrVectFixNAN(filter->output);}

 if(isnormal(filter->reg0) == 0) filter->reg0 = 0;
 if(isnormal(filter->reg1) == 0) filter->reg1 = 0;
 if(isnormal(filter->reg2) == 0) filter->reg2 = 0;

 //--- the next conditions could be triggered by values around 1 droping to 1e-24
 // then the filter output only zeros
 if(filter->reg0 != 0 &&
    filter->reg0 ==  filter->reg2 &&
    filter->a1  == 0 &&
    filter->a2  == 0)  {
   if(fabs((filter->reg0 + filter->reg1) / filter->reg0) < 1.e-15) {
     filter->reg0 = 0;
     filter->reg1 = 0;
     filter->reg2 = 0;
     filter->nResets++;}}

 return(0);

}
/*--------------------------------------------------------- FrvFilter2Init --*/
int FrvFilter2Init(FrvFilter2* filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Initilized the filter object: creat all output structures               */
/*   Returns: 0 in case of success, 1 otherwise                              */
/*---------------------------------------------------------------------------*/
{double fs;

 if(vect == NULL) 
    {sprintf(filter->error,"no input vector");
     return(1);}

 if((vect->type == FR_VECT_C8)  ||
    (vect->type == FR_VECT_C16) ||
    (vect->type == FR_VECT_STRING)) 
    {sprintf(filter->error,"Input type not supported:%d",vect->type);
     return(0);}

 if (filter == NULL) return(1);
 
 if(vect->dx[0] <= 0)
    {sprintf(filter->error,"bad time step:%f",vect->dx[0]);
     return(1);}

 fs = 1. / vect->dx[0];

 filter->alfa2 =   4.*filter->a2*fs*fs  -  2.*filter->a1*fs  +  filter->a0;  
 filter->alfa1 = - 8.*filter->a2*fs*fs  +  2.*filter->a0;  
 filter->alfa0 =   4.*filter->a2*fs*fs  +  2.*filter->a1*fs  +  filter->a0;  
 filter->beta2 =   4.*filter->b2*fs*fs  -  2.*filter->b1*fs  +  filter->b0;  
 filter->beta1 = - 8.*filter->b2*fs*fs  +  2.*filter->b0;  
 filter->beta0 =   4.*filter->b2*fs*fs  +  2.*filter->b1*fs  +  filter->b0;  

 filter->alfa2 =   filter->alfa2 / filter->beta0;
                          
 filter->alfa1 =   filter->alfa1 / filter->beta0;
 filter->alfa0 =   filter->alfa0 / filter->beta0;
 filter->beta2 = - filter->beta2 / filter->beta0;
 filter->beta1 = - filter->beta1 / filter->beta0;

 filter->reg2  = 0.;
 filter->reg1  = 0.;
 filter->reg0  = 0.;

          /*---- init for output space ----*/

 if(filter->outputIsInput == FR_YES) {
   filter->output = vect;
   return(0);}

 filter->output  = FrVectNew1D(vect->name, FR_VECT_8R, vect->nData, 
                               vect->dx[0], "time [s]",vect->unitY);
 if(filter->output  == NULL) 
    {sprintf(filter->error,"malloc in/out failed:ndata=%ld",vect->nData);
     return(1);}

 return(0);

}
/*---------------------------------------------------------- FrvFilter2New --*/
FrvFilter2* FrvFilter2New(double a2,
                          double a1,
                          double a0,
                          double b2,
                          double b1,
                          double b0)
/*---------------------------------------------------------------------------*/
{
 FrvFilter2 *filter;

      /*-------- create the filter structure ----*/

 filter = (FrvFilter2 *) (calloc(1,sizeof(FrvFilter2)));
 if(filter == NULL) return(NULL);
 
 filter->a2 = a2;
 filter->a1 = a1;
 filter->a0 = a0;
 filter->b2 = b2;
 filter->b1 = b1;
 filter->b0 = b0;

 filter->nCall = 0;

 filter->outputIsInput = FR_NO;

 return(filter);
}


