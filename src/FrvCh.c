/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*--------------  FrvCh.c by B.Mours May 31, 2011---------------------------*/

#include "FrvCh.h"
#include <string.h>

/*---------------------------------------------------------------------------*/

/*------------------------------------------------------------- FrvChFree --*/
void  FrvChFree(struct FrvCh* buffer)
/*---------------------------------------------------------------------------*/
{
  if(buffer == NULL) return;
  
  free(buffer->name);
  free(buffer);
 
  return;
}

/*------------------------------------------------------------ FrvChFeed ---*/
double FrvChFeed(FrvCh *buf, 
              FrameH *frame)
/*---------------------------------------------------------------------------*/
{int i, len;
 double value;
 FrVect *vect;
 FrSerData *ser;

 if(buf == NULL) return(-2);
 if(frame == NULL)  return(-3);

            /*---------------------init: is channel a FrVect or SerData?  --*/

 if(buf->init == FR_NO)
   {if(buf->duration < frame->dt) buf->duration = frame->dt;
    buf->dt = frame->dt;

   if((vect = FrameFindVect(frame, buf->name)) != NULL) 
     {buf->buffer = FrvBufNew(vect->nData*buf->duration/frame->dt,
			     vect->nData, vect->type, 0, 0);
      if(buf->buffer == NULL) return(-5);
      buf->init = FR_YES;
      buf->isSerData = FR_NO;}
   else
     {len = strlen(buf->name);
      for(i=1; i<strlen(buf->name); i++)
         {if(buf->name[i] != '_') continue;
          buf->name[i] = '\0';
          buf->smsParam = buf->name+i+1;
          if(FrSerDataGet(frame, buf->name, buf->smsParam,&value) == 0) break;
           buf->name[i] = '_';}
      if(i != len) 
        {ser = FrSerDataFind(frame, buf->name, NULL);
         if(ser->sampleRate != 0) buf->dt = (.001 + 1./ser->sampleRate);
         if(buf->duration < buf->dt) buf->duration = buf->dt;
         buf->name[i] = '_';
         buf->output = FrVectNewTS(buf->name, ser->sampleRate, 
                                buf->duration*ser->sampleRate, -64);
         buf->name[i] = '\0';
         if(buf->output == NULL) return(-4);
         buf->init = FR_YES;
         buf->isSerData = FR_YES;}}
 
    if(buf->init == FR_NO) return(-1);} 



             /*------------------------- find the data for this frame -----*/

 if(buf->isSerData == FR_NO)               /*----- ADC, Proc, ... case ----*/
   {vect = FrameFindVect(frame, buf->name);
    if(vect == NULL) return(-1);
    FrvBufFeed(buf->buffer, vect);
    FrvBufGetNext(buf->buffer);
    buf->output = buf->buffer->output;} 
 else if((frame->GTimeS % buf->dt) != 0)  /*--- SerData not at frame rate---*/
   {if(frame->GTimeS > buf->lastGPS + .1) buf->stillToFeed = buf->duration;
    return(buf->stillToFeed);}
 else                                     /*---- SerData at frame rate------*/
   {if(FrSerDataGet(frame, buf->name, buf->smsParam, &value) != 0) return(-1);
    for(i=0; i<buf->output->nData-1; i++)
      {buf->output->dataD[i] = buf->output->dataD[i+1];}
    buf->output->dataD[buf->output->nData-1] = value;}

                                 /*--------------------- continuity check --*/

 if((buf->lastGPS != 0) &&
    (abs(buf->lastGPS - frame->GTimeS) > 1.e-5)) 
       {buf->stillToFeed = buf->duration;}

 buf->lastGPS = frame->GTimeS + buf->dt;
 buf->stillToFeed -= buf->dt;
 if(buf->stillToFeed < 0) buf->stillToFeed = 0;
    
 return(buf->stillToFeed);
}

 
/*------------------------------------------------------------ FrvChGetNext-*/
FrVect* FrvChGetVect(FrvCh *buffer)
/*---------------------------------------------------------------------------*/
/*  Returns: the pointer to the output vector or NULL if additional call to
             to FrvChFeed are needed.                                        */
/*---------------------------------------------------------------------------*/
{
  if(buffer->stillToFeed != 0) 
       return(NULL);
  else return(buffer->output);
}

/*-------------------------------------------------------------- FrvChNew --*/
FrvCh* FrvChNew(char *name,
                double duration)
/*---------------------------------------------------------------------------*/
/* Create the buffer structure                                               */
/*---------------------------------------------------------------------------*/
{FrvCh *buffer;

 buffer = (struct FrvCh *) (calloc(1,sizeof(struct FrvCh)));
 if(buffer == NULL) return(NULL);

 FrStrCpy(&(buffer->name),name);
  
 buffer->duration    = duration;
 buffer->stillToFeed = duration;
 buffer->init        = FR_NO;
 buffer->isSerData   = FR_NO;
 buffer->output      = NULL;
 
 return(buffer);
}
