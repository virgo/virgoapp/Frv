/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*  Author : D. Buskulic, F. Marion and B.Mours LAPP (Annecy) May 31, 2011*/
/*           and Leone Bosi for FFTW3 implementation                   */ 
/*//////////////////////////////////////////////////////////////////// */
/*                                                                     */
/*   Fast Fourier Transform using the FFTW3 package                    */
/*                                                                     */
/*//////////////////////////////////////////////////////////////////// */

#include <string.h>
#include <math.h>
#include "FrvFFT.h"
#include "fftw3.h"

void   FrvRFFTdoD  (FrvRFFT *fft, FrVect *vect);
void   FrvRFFTdoOne(FrvRFFT *fft, FrVect *vect);

/* The following two functions were adapted from AverageSpectrum.c in the lal suite */

static int compare_double( const void *p1, const void *p2 )
{
  double x1 = *(const double *)p1;
  double x2 = *(const double *)p2;
  return (x1 > x2) - (x1 < x2);
}
double MedianBias( int nn )
{
  const int nmax = 1000;
  double ans = 1;
  int n = (nn - 1)/2;
  int i;

  if ( nn >= nmax )
    return 0.693147180559945309417232121458176568;

  for ( i = 1; i <= n; ++i )
    {
      ans -= 1.0/(2*i);
      ans += 1.0/(2*i + 1);
    }

  return ans;
}

/*-----------------------------------------------------------FrvBandRmsFree--*/
void  FrvBandRmsFree(FrvBandRms *rms)
/*---------------------------------------------------------------------------*/
{
 if(rms == NULL) return;

 if(rms->channel != NULL) free(rms->channel);
 if(rms->fft != NULL) FrvRFFTFree(rms->fft);
 free(rms);

 return;}

/*------------------------------------------------------------FrvBandRmsNew--*/
FrvBandRms* FrvBandRmsNew(char *channel,
                      double FFTduration,
                      int    nAverage,
                      double freqMin,
                      double freqMax)
/*---------------------------------------------------------------------------*/
{FrvBandRms *rms;

 rms = (FrvBandRms *) (calloc(1, sizeof(FrvBandRms)));
 if(rms == NULL) return(NULL);

 FrStrCpy(&(rms->channel),channel);
 rms->nAverage    = nAverage;
 rms->freqMin     = freqMin;
 rms->freqMax     = freqMax;

 rms->fft = FrvRFFTNewT("HAON", FFTduration, nAverage);
           
 return(rms);
}
/*-----------------------------------------------------------FrvBandRmsProc--*/
int FrvBandRmsProc(FrvBandRms* rms, FrameH *frame)
/*---------------------------------------------------------------------------*/
{ FrVect *vect, *amplitude;
  int i, iMin, iMax, nMissing;
  double sum, amp;

  if(rms == NULL) return(-1);

  vect = FrameFindVect(frame, rms->channel);
  if(vect == NULL) return(-2);

       /*--------------------------------------------- process the data -----*/

  FrvRFFTFor(rms->fft, vect);
  nMissing = rms->nAverage - rms->fft->nFFT;
  if(nMissing > 0) return(nMissing);

       /*------------------------- compute the rms in the frequency band ----*/

  amplitude = rms->fft->amplitudeA;
  iMin = FrVectGetIndex(amplitude, rms->freqMin);
  iMax = FrVectGetIndex(amplitude, rms->freqMax);
  sum    = 0;
  for(i=iMin; i<=iMax; i++)
    {amp = amplitude->dataD[i];
     sum += amp*amp;}

  rms->rms = sqrt(sum/rms->fft->duration);
  rms->GTime = frame->GTimeS+1.e-9*frame->GTimeN;
 
  return(0);}

/*------------------------------------------------------------- FrvRFFTdoD --*/
void FrvRFFTdoD(FrvRFFT *fft, FrVect *vect)
/*---------------------------------------------------------------------------*/
{int i, nData;
 double *input, *temp,  mean, *window, *output, coef, dt;
 float *inputF, *tempF;

         /*---------------------------------------------- prepare data ------*/
         /* remark: the output buffer is used to prepare the input data ...  */

 nData  = vect->nData;
 input  = fft->output->dataD;  
 output = fft->output->dataD;
 temp   = fft->temp->dataD;  
 tempF  = (float*) temp;
 inputF = (float*) input;

         /*---------------------------------------- extract input data-------*/

 if(vect->type == FR_VECT_2S)
       {for(i=0; i<nData; i++) {input[i] = vect->dataS[i];}}
 else if(vect->type == FR_VECT_4R)
       {for(i=0; i<nData; i++) {input[i] = vect->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
       {for(i=0; i<nData; i++) {input[i] = vect->dataI[i];}}
 else if(vect->type == FR_VECT_8R)
       {for(i=0; i<nData; i++) {input[i] = vect->dataD[i];}}
     
         /*----------------- compute and supress the mean value if needed----*/

 mean = 0.; 
 if(fft->optionP == FR_YES)
   {for(i=0; i<nData; i++) {mean += input[i];}
    mean = mean/nData;
    fft->mean = mean;}

         /*------------------------------- prepare input data with window ---*/

 if((fft->optionP == FR_YES) && 
    (fft->optionH == FR_YES))
   {window = fft->window->dataD;
    for(i=0; i<nData; i++) 
      {input[i] = window[i]*(input[i] - mean);}}

 else if(fft->optionP == FR_YES)
   {for(i=0; i<nData; i++)
     {input[i] = input[i] - mean;}}

 else if(fft->optionH == FR_YES)
   {window = fft->window->dataD;
   for(i=0; i<nData; i++) input[i] = window[i]*input[i];}

         /*----------------------------------convert to float if requested---*/

 if(fft->optionF == FR_YES)
   {for(i=0; i<nData; i++) inputF[i] = input[i];}

         /*---------------------------------------------------- call fft ----*/

 fftw_execute_r2r((fftw_plan) fft->plan, input, temp);

         /* -------------------------------get the normalisation factor -----*/

 dt = vect->dx[0];
 if(vect->nDim == 2) dt = vect->dx[1]; /*- this is for the ADC sample data---*/

 if(fft->optionN == FR_YES)
   {if(fft->buffer == NULL)
     {coef = sqrt(2.)*sqrt(nData*dt*fft->decimate)/nData;}
   else
     {coef = sqrt(2.)*sqrt(nData*dt)/nData;}}
 else coef = sqrt(2.)/nData;

          /* - copy the result to the FrVect complex order if requested -----*/

 if(fft->optionR == FR_YES && fft->optionF == FR_YES)
   {for (i=0;i<nData;i++)
      {output[i] = coef*tempF[i];}}
 else if(fft->optionR == FR_YES)
   {for (i=0;i<nData;i++)
      {output[i] = coef*temp[i];}}
 else if(fft->optionF == FR_YES)
   {for (i=0;i<((nData+1)/2)-1;i++)
      {output[2*i]   = coef*tempF[i+1];
       output[2*i+1] = coef*tempF[nData-i-1];}

    fft->mean = coef*tempF[0];    /*--- DC component ---*/

    if (nData % 2 ==0)
      {output[nData-2] = coef*tempF[nData/2];   /* Nyquist frequency */
       output[nData-1] = 0.;}}
 else
   {for (i=0;i<((nData+1)/2)-1;i++)
      {output[2*i]   = coef*temp[i+1];
       output[2*i+1] = coef*temp[nData-i-1];}

    fft->mean = coef*temp[0];    /*--- DC component ---*/

    if (nData % 2 ==0)
      {output[nData-2] = coef*temp[nData/2];   /* Nyquist frequency */
       output[nData-1] = 0.;}}

  return;
}

/*----------------------------------------------------------- FrvRFFTdoOne --*/
void FrvRFFTdoOne(FrvRFFT *fft, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Internal function. Apply one FFT to the input vector                    */
/*   Returns the result in the FrvRFFT structure as output                   */
/*   If on input fft=0, builds a standard fft structure from the vector vect */
/*---------------------------------------------------------------------------*/
{
  int i, j, nData, nAvailable, nAvailableEven, nAvailableOdd, nLeft;
  double *amplitude, *amplitudeA, *output, decay;
  double bias, biasEven, biasOdd, inverseBias, inverseBiasEven, inverseBiasOdd;

          /*-------------------- set start time for all the series defined --*/

 fft->output->GTime  = vect->GTime;
 if(fft->amplitude  != NULL) fft->amplitude ->GTime = vect->GTime;
 if(fft->amplitudeA != NULL) fft->amplitudeA->GTime = vect->GTime;

          /*-------------------------------------------compute the FFT  -----*/
 
 FrvRFFTdoD(fft, vect);
 fft->nFFT++;
 
 nData  = vect->nData;
 output    = fft->output->dataD;

          /*--------------------------------------------- compute amplitude--*/

 if(fft->optionS == FR_YES)
   {amplitude  = fft->amplitude->dataD;
    /* Will go from power to amplitude later */
    for(i=0; i<nData/2; i++)
      {amplitude[i] = output[2*i]*output[2*i] + output[2*i+1]*output[2*i+1];}

         /*------------------------------------ compute average amplitude ---*/

    if(fft->optionA == FR_YES)
      {amplitudeA = fft->amplitudeA->dataD;
       if(fft->nFFT == 1)
         {for(i=0; i<nData/2; i++) {amplitudeA[i] =  sqrt(amplitude[i]);}}
       else
         {if(fft->nFFT < fft->nAverage) decay = 1.-1./fft->nFFT;
          else                          decay = 1.-1./fft->nAverage;
          for(i=0; i<nData/2; i++)
            {amplitudeA[i] =  sqrt(decay*amplitudeA[i]*amplitudeA[i] + 
                          (1.-decay)*amplitude[i]);}}}}

 /*------------------------------------ compute median amplitude ---*/

 if(fft->optionM == FR_YES)
   {amplitudeA = fft->amplitudeA->dataD;
    amplitude  = fft->amplitude->dataD;
    nAvailable = fft->nAverage;
    nLeft = fft->nFFT % fft->medianPeriod;

    if(fft->nFFT < nAvailable) nAvailable = fft->nFFT+1;

    if(!(fft->optionO))
       {bias = MedianBias(nAvailable);
	 inverseBias = 1./bias;

	 for(i=0; i<nData/2; i++) 
	   {fft->array_for_median[i][fft->nFFT%fft->nAverage] =  amplitude[i];
            if(nLeft != 0) continue; /* do not get the median at each new FFT*/   

	     for(j=0; j<nAvailable; j++) fft->array_for_sorting[j] = fft->array_for_median[i][j];
	     /* sort and find median */
	     qsort(fft->array_for_sorting, nAvailable, sizeof(double), compare_double);
	     if(nAvailable%2) /* odd number of samples */
	       {amplitudeA[i] = fft->array_for_sorting[nAvailable/2];}
	     else /* even number of samples, take average */
	       {amplitudeA[i] = 0.5*(fft->array_for_sorting[nAvailable/2-1] 
				     + fft->array_for_sorting[nAvailable/2]);}
	     /* remove median bias and go from power to amplitude */
	     amplitudeA[i] = sqrt(inverseBias*amplitudeA[i]);
	   }
       }

     else 
       {/* Average median on even samples and median on odd samples to avoid bias from non 
	   independent segments	due to overlap (from method used in AverageSpectrum.c in the lal suite) */

	 /* compute median with even samples */
	 if(nAvailable%2) nAvailableEven = nAvailable/2 + 1;
	 else nAvailableEven = nAvailable/2;
	 nAvailableOdd = nAvailable/2;
	 biasEven = MedianBias(nAvailableEven);
	 inverseBiasEven = 0.5/biasEven;
	 biasOdd = MedianBias(nAvailableOdd);
	 inverseBiasOdd = 0.5/biasOdd;
	 
	 for(i=0; i<nData/2; i++)
	   {fft->array_for_median[i][fft->nFFT%fft->nAverage] =  amplitude[i];
            if(nLeft != 0) continue; /* do not get the median at each new FFT*/   
	     
	     /* sort and find median for even samples */
	     
	     for(j=0; j<nAvailable; j=j+2) fft->array_for_sorting[j/2] = fft->array_for_median[i][j];
	     qsort(fft->array_for_sorting, nAvailableEven, sizeof(double), compare_double);
	     if(nAvailableEven%2) /* odd number of samples */
	       {amplitudeA[i] = inverseBiasEven*fft->array_for_sorting[nAvailableEven/2];}
	     else /* even number of samples, take average */
	       {amplitudeA[i] = inverseBiasEven*0.5*(fft->array_for_sorting[nAvailableEven/2-1]
						     + fft->array_for_sorting[nAvailableEven/2]);}
	     
	     /* sort and find median for odd samples and average with median for even samples */
	     for(j=1; j<nAvailable; j=j+2) fft->array_for_sorting[j/2] = fft->array_for_median[i][j];
	     qsort(fft->array_for_sorting, nAvailableOdd, sizeof(double), compare_double);
	     if(nAvailableOdd%2) /* odd number of samples */
	       {amplitudeA[i] += inverseBiasOdd*fft->array_for_sorting[nAvailableOdd/2];}
	     else /* even number of samples, take average */
	       {amplitudeA[i] += inverseBiasOdd*0.5*(fft->array_for_sorting[nAvailableOdd/2-1]
						     + fft->array_for_sorting[nAvailableOdd/2]);}
	     /* Go from power to amplitude */
	     amplitudeA[i] = sqrt(amplitudeA[i]);
	   }
       }
   }

 if(fft->optionS == FR_YES)
   {nData  = vect->nData;
     amplitude = fft->amplitude->dataD;
     for(i=0; i<nData/2; i++) amplitude[i] = sqrt(amplitude[i]);
   }

          /*------------------- set start time for all the series defined ---*/

 fft->output->GTime = vect->GTime;
 if(fft->amplitude  != NULL) fft->amplitude ->GTime = vect->GTime;
 if(fft->amplitudeA != NULL) fft->amplitudeA->GTime = vect->GTime;
 

 return;
}

/*------------------------------------------------------------ FrvRFFTFree --*/
void  FrvRFFTFree(FrvRFFT* fft)
/*---------------------------------------------------------------------------*/
{int i;

  if(fft == NULL) return;
 
  FrVectFree(fft->output);

  if(fft->window != NULL) FrVectFree(fft->window);
  if(fft->amplitude  != NULL) FrVectFree(fft->amplitude);
  if(fft->amplitudeA != NULL) FrVectFree(fft->amplitudeA);
  if(fft->temp       != NULL) FrVectFree(fft->temp);
  if(fft->buffer     != NULL) FrvBufFree(fft->buffer);

  if(fft->array_for_median != NULL) 
    {for(i=0; i<fft->fftSize/2; i++) free(fft->array_for_median[i]);
      free(fft->array_for_median);}
  if(fft->array_for_sorting != NULL) free(fft->array_for_sorting);

  fftw_destroy_plan((fftw_plan) fft->plan);

  if(fft->name != NULL) free(fft->name);

  free(fft);

  return;
}
/*------------------------------------------------------------- FrvRFFTFor --*/
FrvRFFT* FrvRFFTFor(FrvRFFT *fft, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the FFT algorithm "fft" to the vector "vect"                    */
/*   Returns the result in the FrvRFFT structure as output                   */
/*   If on input fft=NULL, builds a standard fft object from the vector vect */
/*---------------------------------------------------------------------------*/
{
 if(vect == NULL)  return(NULL);

           /*-- If no fft algorithm object on input, build a standard one ---*/

 if(fft  == NULL)  
   {fft = FrvRFFTNew("AHNS", 0, 0);
    if(fft == NULL) return(NULL);}

 if(fft->output == NULL)
   {if(FrvRFFTInit(fft, vect) != 0) return(NULL);}

           /*-----------check that we work on the same kind of vector -------*/

 if(fft->nData != vect->nData) 
   {sprintf(fft->err," nData missmatch:%ld %ld\n",fft->nData,vect->nData);
    return(NULL);}

         /*--------- Case when the FFT is done on the raw input vector ------*/

 if(fft->buffer == NULL)
   {FrvRFFTdoOne(fft, vect);}

         /*---------------- Loop over the new resized vectors ---------------*/

 else
   {int irc = FrvBufFeed(fft->buffer,vect);
    if(irc != 0) return(fft);

    while(FrvBufGetNext(fft->buffer) == 0) 
      {FrvRFFTdoOne(fft, fft->buffer->output);}}

 return(fft);
}

/*------------------------------------------------------------ FrvRFFTInit --*/
int FrvRFFTInit(FrvRFFT* fft, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Initialized the fft object: create all output structures                */
/*   Returns: 0 in case of success, 1 otherwise                              */
/*---------------------------------------------------------------------------*/
{int i, step;
 double a, b, c;
 char name[512],unitY[256], uY[300];

 if(fft  == NULL)  return(1);
 if(vect == NULL) 
    {sprintf(fft->err,"no input vector");
     return(1);}

 if((vect->type != FR_VECT_2S) &&
    (vect->type != FR_VECT_4R) &&
    (vect->type != FR_VECT_4S) &&
    (vect->type != FR_VECT_8R)) 
    {sprintf(fft->err,"input type not supported:%d",vect->type);
     return(1);}

 fft->nData  = vect->nData;
        
 FrStrCpy(&fft->name, vect->name);

            /*----------------- init for the intermediate buffer ------------*/

 if(fft->duration != 0) 
   {fft->fftSize = (fft->duration+.5*vect->dx[0])/vect->dx[0];}
 else
   {if(fft->fftSize <= 0) fft->fftSize = vect->nData;
    fft->duration = fft->fftSize*vect->dx[0];}

 if((fft->fftSize != vect->nData) ||
    (fft->decimate > 1) ||
    (fft->optionO == FR_YES))
   {if(fft->optionO == FR_YES)
         {step = fft->fftSize/2;}
    else {step = fft->fftSize;}
    fft->buffer = FrvBufNew(fft->fftSize, step, FR_VECT_8R,fft->decimate, 0);
    if(fft->buffer == NULL) return(1);
    FrvBufSetAligned(fft->buffer, fft->gpsOffset);}
 else
   {fft->buffer = NULL;}

           /*--------------------- init for the output space ----------------*/

 fft->dt     = vect->dx[0]*fft->decimate;
 fft->dnu    = 1./(fft->fftSize*fft->dt);

 sprintf(name,"FFT(%s)",vect->name);
 if(vect->unitY  == NULL)   
        sprintf(unitY," ");
 else   sprintf(unitY,"%s",vect->unitY);
 if(fft->optionN == FR_YES) 
         sprintf(uY,"%s/sqrt(Hz)",unitY);
 else    sprintf(uY,"%s",unitY);

 if(fft->optionR == FR_YES)
   {fft->output = FrVectNew1D(name, FR_VECT_16H,fft->fftSize,
                            fft->dnu, "frequency[Hz]",uY);}
 else
   {fft->output = FrVectNew1D(name, FR_VECT_16C,fft->fftSize/2, 
                            fft->dnu, "frequency[Hz]",uY);}
 if(fft->output  == NULL) 
    {sprintf(fft->err,"malloc in/out failed:fftSize=%d",fft->fftSize);
     return(1);}
  
          /*-------------------------------- allocate work space ------------*/

 fft->temp = FrVectNew1D(name, FR_VECT_8R,fft->fftSize,fft->dnu, NULL, NULL);
 if(fft->temp  == NULL) 
    {sprintf(fft->err,"malloc workspace failed:fftSize=%d",fft->fftSize);
     return(1);}
    
          /*------------------------- init for the window -------------------*/
          /*----- the normalization do not change the spectrum amplitude ----*/

 if(fft->optionH == FR_YES)
   {fft->window  = FrVectNew1D("window",FR_VECT_8R, fft->fftSize,
                                fft->dt, "time","window");
    if(fft->window  == NULL) 
       {sprintf(fft->err,"malloc window failed:ndata=%d",fft->fftSize);
        return(1);}

    a = fft->fftSize - 1.;
    b = 6.2831853071795864769/a;
    c = 1./sqrt(1.5);

    for (i=0; i<fft->fftSize; i++)
      {fft->window->dataD[i] = c*(1. + cos(b*((double)i - a*.5)));}}

           /*----------------- init for the amplitude spectrum---------------*/

 if(fft->optionS == FR_YES)
   {sprintf(name,"FFT amplitude(%s)",vect->name);
    fft->amplitude  = FrVectNew1D(name, FR_VECT_8R, fft->fftSize/2,
                                  fft->dnu,"frequency[Hz]",uY);
    if( fft->amplitude  == NULL)  
       {sprintf(fft->err,"malloc amplitude failed:ndata=%d",fft->fftSize);
        return(1);}}
      
          /*------------------ init for the average amplitude ---------------*/

 if(fft->optionA == FR_YES)
   {sprintf(name,"FFT <amplitude>(%s)",vect->name);
    if(fft->optionN == FR_YES)
          sprintf(uY,"<%s/sqrt(Hz)>",unitY);
    else  sprintf(uY,"<%s>",unitY);
    fft->amplitudeA  = FrVectNew1D(name,  FR_VECT_8R, fft->fftSize/2,
                                   fft->dnu,"frequency[Hz]",uY);
    if( fft->amplitudeA  == NULL)  
       {sprintf(fft->err,"malloc amplitudeA failed:ndata=%d",fft->fftSize);
        return(1);}
 
    for (i=0; i<fft->fftSize/2; i++)
       {fft->amplitudeA->dataD[i] = 0.;}
   }

 /*------------------ init for the median amplitude ---------------*/
 if(fft->optionM == FR_YES)
   {sprintf(name,"FFT <amplitude>(%s)",vect->name);
     if(fft->optionN == FR_YES)
       sprintf(uY,"<%s/sqrt(Hz)>",unitY);
     else  sprintf(uY,"<%s>",unitY);
     fft->amplitudeA  = FrVectNew1D(name,  FR_VECT_8R, fft->fftSize/2,
				    fft->dnu,"frequency[Hz]",uY);
     if( fft->amplitudeA  == NULL)
       {sprintf(fft->err,"malloc amplitudeA failed:ndata=%d",fft->fftSize);
	 return(1);}

     for (i=0; i<fft->fftSize/2; i++)
       {fft->amplitudeA->dataD[i] = 0.;}

     fft->array_for_median = calloc(fft->fftSize/2, sizeof(double*));
     if(fft->array_for_median == NULL) return(1);
     for (i=0; i<fft->fftSize/2; i++) 
       {fft->array_for_median[i] = calloc(fft->nAverage, sizeof(double));
	 if(fft->array_for_median[i] == NULL) return(1);}
     fft->array_for_sorting = calloc(fft->nAverage, sizeof(double));
     if(fft->array_for_sorting == NULL) return(1);
   } 
          /*--------------------- creat FFTW plan  --------------------------*/

 if(fft->optionF == FR_YES)
   fft->plan = (void *) fftwf_plan_r2r_1d(fft->fftSize,
                             (float*) fft->output->dataD,
                             (float*) fft->temp->dataD,
                             FFTW_R2HC, FFTW_ESTIMATE);
 else
   fft->plan = (void *) fftw_plan_r2r_1d(fft->fftSize,
                             fft->output->dataD, fft->temp->dataD,
                             FFTW_R2HC, FFTW_ESTIMATE);
 
 if(fft->plan == NULL) return(1);

         /*-------------------- adjust bin origin ---------------------------*/

 if(fft->optionR == FR_YES)
      {fft->output->startX[0] =-0.5*fft->dnu;}
 else {fft->output->startX[0] = 0.5*fft->dnu;}
 if(fft->amplitude  != NULL)  fft->amplitude->startX[0] = 0.5*fft->dnu;
 if(fft->amplitudeA != NULL) fft->amplitudeA->startX[0] = 0.5*fft->dnu;

 return(0);

}
/*------------------------------------------------------------- FrvRFFTNew --*/
FrvRFFT* FrvRFFTNew(char* option, int fftSize, int decimate)
/*---------------------------------------------------------------------------*/
/*   Returns the FrvRFFT structure as output            

  option characters: 
   H  to apply an hanning window (normalized to not change the amplitude)
   P  to supress the pedestal (average value) 
   S  to compute the spectrum (amplitude)
   A  to compute the average spectrum
   M  to compute the median spectrum
   N  to normalized the result as if it's noise (i.e. unit are  by sqrt(Hz)). 
      The default units are absolute units (i.e. the amplitude of a sine wave
      is independant of the FFT length.
   R  to keep the raw hermitian order (incompatible with the S option)

   option = "HS" means hanning window+amplitude spectrum computed            */
/*---------------------------------------------------------------------------*/
{
 FrvRFFT *fft;

            /*------------------- create the fft structure ------------------*/

 fft = (FrvRFFT *) (calloc(1,sizeof(FrvRFFT)));
 if(fft == NULL) return(NULL);
 
 sprintf(fft->err,"no error");

 fft->fftSize  = fftSize;
 if(decimate < 1) decimate = 1;
 fft->decimate = decimate;
 fft->nAverage = 10000000.;
 fft->nFFT = 0;
 fft->medianPeriod = 1;
 fft->gpsOffset = -1;

            /*------------------------ decode options -----------------------*/

 fft->optionA = FR_NO;
 fft->optionM = FR_NO;
 fft->optionH = FR_NO;
 fft->optionO = FR_NO;
 fft->optionN = FR_NO;
 fft->optionP = FR_NO;
 fft->optionS = FR_NO;
 fft->optionR = FR_NO;
 fft->optionF = FR_NO;
 if(option != NULL)
   {if(strchr(option,'A') != NULL) fft->optionA = FR_YES;
    if(strchr(option,'M') != NULL) fft->optionM = FR_YES;
    if(strchr(option,'H') != NULL) fft->optionH = FR_YES;
    if(strchr(option,'O') != NULL) fft->optionO = FR_YES;
    if(strchr(option,'N') != NULL) fft->optionN = FR_YES;
    if(strchr(option,'P') != NULL) fft->optionP = FR_YES;
    if(strchr(option,'S') != NULL) fft->optionS = FR_YES;
    if(strchr(option,'R') != NULL) fft->optionR = FR_YES;
    if(strchr(option,'F') != NULL) fft->optionF = FR_YES;}

 if(fft->optionA == FR_YES) fft->optionS = FR_YES;
 if(fft->optionM == FR_YES) fft->optionS = FR_YES;
 if(fft->optionM == FR_YES) fft->optionA = FR_NO;
 if(fft->optionR == FR_YES) fft->optionS = FR_NO;

 return(fft);

}
/*------------------------------------------------------------- FrvRFFTNewT--*/
FrvRFFT* FrvRFFTNewT(char* option, double duration, int nAverage)
/*---------------------------------------------------------------------------*/
/*   Build the FFT object using seconds and not number of samples            */
/*---------------------------------------------------------------------------*/
{
 FrvRFFT *fft;

 fft = FrvRFFTNew(option, 0, 0);
 if(fft == NULL) return(NULL);

 fft->duration = duration;
 if (nAverage < 1) nAverage = 1;
 fft->nAverage = nAverage;

 return(fft);
}

/*---------------------------------------------------------------------------*/
void FrvRFFTSetAligned(FrvRFFT* fft, int offset)
/*---------------------------------------------------------------------------*/
{
 if(fft != NULL) fft->gpsOffset = offset;

 return;
}

/*---------------------------------------------------------------------------*/
int FrvRFFTSetMedianPeriod(FrvRFFT* fft, int medianPeriod)
/*---------------------------------------------------------------------------*/
{
 if(medianPeriod > 0) fft->medianPeriod = medianPeriod;

 return(fft->medianPeriod);
}
/*------------------------------------------------------------- FrvFFTD704 --*/
void FrvFFTD704(double *a, long msign)
/*---------------------------------------------------------------------------*/
/* cdfft computes the finite Fourier transform of a complex periodic         */
/* sequence, whose period n must be a power of 2 (n=2**|msign|).             */
/* To ensure optimum use of storage, the same array is used for input        */
/* and output. If msign is positive the direct Fourier transform is          */
/* computed. If msign is negative, the inverse Fourier transform is          */
/* computed. This code is a c translation of the routine D704 (CFFT)         */
/* from CERNLIB. See the CERN writeup for more information.                  */
/* Author: B. Mours        27/02/91                                          */
/*---------------------------------------------------------------------------*/
 
{
 double c,s,ur,ui,ur0,wr,wi,tr,ti,temp;
 long  m,n,nv2,nm1,i,j,k,le,l,le1,ip;
 long indexI,indexJ,indexIp;
 
 if (msign == 0) return;
 m   = msign;
 if (msign < 0) m = -m;
 n   = 1;
 for (i=0; i<m; i++) {n=2*n;}
 nv2 = n/2;
 nm1 = n-1;
 j   = 0;
 
 for (i=0; i<nm1; i++)
    {indexI = 2*i;
     if (i < j)
        {indexJ      = 2*j;
         tr          = a[indexJ  ];
         ti          = a[indexJ+1];
         a[indexJ  ] = a[indexI  ];
         a[indexJ+1] = a[indexI+1];
         a[indexI  ] = tr;
         a[indexI+1] = ti;
        }
     k = nv2-1;
     while ( k < j )
        {j = j - k - 1;
         k = (k-1) / 2;
        }
    j = j + k + 1;
   }
 
 for (i=0; i<n; i=i+2)
    {indexI       = 2*i;
     tr           = a[2*(i+1)];
     ti           = a[2*(i+1)+1];
     a[2*(i+1)  ] = a[indexI  ] - tr;
     a[2*(i+1)+1] = a[indexI+1] - ti;
     a[indexI  ]  = a[indexI  ] + tr;
     a[indexI+1]  = a[indexI+1] + ti;
    }
 
 if (m == 1) return;
 c = 0;
 s = 1.;
 if (msign < 0) s = -1.;
 le = 2;
 
 for (l=2; l<=m; l++)
    {wr  = c;
     wi  = s;
     ur  = wr;
     ui  = wi;
     c   = sqrt(c*.5+.5);
     s   = wi/(c+c);
     le1 = le;
     le  = le1+le1;
 
     for (i=0; i<n; i=i+le)
        {ip      = i + le1;
         indexI  = 2*i;
         indexIp = 2*ip;
         tr      = a[indexIp];
         ti      = a[indexIp+1];
         a[indexIp  ] = a[indexI  ] - tr;
         a[indexIp+1] = a[indexI+1] - ti;
         a[indexI  ]  = a[indexI  ] + tr;
         a[indexI+1]  = a[indexI+1] + ti;
        }

     for (j=1; j<le1; j++)
        {for (i=j; i<n; i=i+le)
            {ip      = i + le1;
             indexI  = 2*i;
             indexIp = 2*ip;
             tr      = a[indexIp  ]*ur - a[indexIp+1]*ui;
             ti      = a[indexIp+1]*ur + a[indexIp  ]*ui;
             a[indexIp  ] = a[indexI  ] - tr;
             a[indexIp+1] = a[indexI+1] - ti;
             a[indexI  ]  = a[indexI  ] + tr;
             a[indexI+1]  = a[indexI+1] + ti;
            }
 
         ur0 = ur;
         ur  = ur*wr - ui*wi;
         ui  = ui*wr + ur0*wi;
        }
    }
 
 for (i=1; i<n/2; i++)
    {temp         = a[2*i];
     a[2*i]       = a[2*(n-i)];
     a[2*(n-i)]   = temp;
     temp         = a[2*i+1];
     a[2*i+1]     = a[2*(n-i)+1];
     a[2*(n-i)+1] = temp;
    }

 return;
}
