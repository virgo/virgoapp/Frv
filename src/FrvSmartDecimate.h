/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/***************************************************************************
                          FrvSmartDecimate.h  -  description
                             -------------------
    begin                : Fri Mar 1 2002
    copyright            : (C) 2002 by Isidoro Ferrante
    email                : ferrante@df.unipi.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef FRVSMARTD
#define FRVSMARTD

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
#include <FrameL.h>
#include "FrvLinFilt.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FrvSmartD FrvSmartD;

struct FrvSmartD {
  int factor;
  int nsteps;
  int *steps;
  /*
  FrvLinFilt ** bank[10];
  */
  FrvLinFilt ** bank;
  FrVect ** vectors;
  char error[256];  
  FrVect * input;
  FrVect * output;
  char *name;      /* channel name for this filter */
  FrvSmartD *next; /* next object in a linked list */
};

FrvSmartD *FrvSmartDInit(int factor,double f0,double fmax, double tp, double treject);
void FrvSmartDFree(FrvSmartD* filterbank);
FrVect * FrvSmartDProc(FrvSmartD * bank, FrVect * Vin, FrVect * Vout);
  void FrvSmartDReset( FrvSmartD * filterbank, double value);
#ifdef __cplusplus
}
#endif

#endif
