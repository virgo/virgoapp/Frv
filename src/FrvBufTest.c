/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*---------------------------------------------------------------------------*/
/* FrvBufTest.c      Dec 2, 2002                                             */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "FrvBuf.h"
 
#define NVECT 6
struct FrVect *vect[NVECT];
/*------------------------------------------------------------test function--*/
void test(int outSize, int step, int type, int decimate, int delay)    
/*---------------------------------------------------------------------------*/
{struct FrvBuf *buffer;
 int i, iVect, t;

 printf(" ------------------outSize=%d step=%d type=%d decimate=%d delay=%d\n",
                     outSize,step, type, decimate, delay);

 buffer = FrvBufNew(outSize, step, type, decimate, delay);

 for(iVect=0; iVect<NVECT; iVect++)
   {printf(" Feed next vector\n");

    FrvBufFeed(buffer, vect[iVect]);
    printf(" Still to go:%d\n",FrvBufStillToGo(buffer)); 
    while(FrvBufGetNext(buffer) == 0)
      {printf(" Get new buffer");
       t = buffer->output->type;
       if     (t == FR_VECT_8R) printf("(D):");
       else if(t == FR_VECT_4R) printf("(F):");
       else if(t == FR_VECT_4S) printf("(I):");
       else if(t == FR_VECT_2S) printf("(S):");
       for(i=0; i<buffer->output->nData; i++)  
         {if     (t == FR_VECT_8R) printf(" %.1f",buffer->output->dataD[i]);
          else if(t == FR_VECT_4R) printf(" %.1f",buffer->output->dataF[i]);
          else if(t == FR_VECT_4S) printf(" %d"  ,buffer->output->dataI[i]);
          else if(t == FR_VECT_2S) printf(" %d"  ,buffer->output->dataS[i]);}
       printf(" GTime=%g\n",buffer->output->GTime);}}

 return;}
/*---------------------------------------------------------------------Main--*/
int main(int argc, char *argv[])  
/*---------------------------------------------------------------------------*/
{int nData, i, iVect, value;
 double t, dt;

          /*------------- first fill the input vectors -----*/

 value = 0;
 nData = 8;
 t = 0;
 dt = 1.1;
 for(iVect = 0; iVect<NVECT; iVect++)
   {nData = 8+2*iVect;
    if(iVect == 3) nData = 6;
    vect[iVect] = FrVectNew1D("input",FR_VECT_8R, nData,dt,"","");
    vect[iVect]->GTime = t;
    t += dt*nData;
    for(i=0; i<vect[iVect]->nData; i++) {vect[iVect]->dataD[i] = value++;}
    FrVectDump(vect[iVect],stdout,2);}
 
  test(5,1,-1,0,0);
  test(4,2,-1,0,0);
  test(8,8,-1,0,0);
  test(8,4,FR_VECT_4R,1,0);
  test(8,4,FR_VECT_4R,2,0);
  test(8,4,FR_VECT_4S,2,10);
  test(8,4,FR_VECT_4S,2,0);
  test(12,8,FR_VECT_2S,0,0);
  test(20, 20,-1,0,0);

  return(0);
}
