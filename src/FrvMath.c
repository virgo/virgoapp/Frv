/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#define _POSIX_SOURCE 
/*---------------------------------------------------------------------------*/
/*  FrvMath.c by I. Fiori, I. Ferrante, F. Marion, B.Mours      Jan 07, 2004 */
/*---------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h> 
#include <stdarg.h>
#include <ctype.h>
#include "FrvMath.h"

/*-------------------------------------------------------------------FrvAdd--*/
FrVect *FrvAdd(FrVect *vect1,
               FrVect *vect2,
               FrVect *vect3,
               char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vectOut= vect1+vect2. The vector vectOut could be  */
/* vect1 or vect2. If vectOut = NULL, a new one is created named newName.    */
/* It returns, the output vector or NULL in case of error.                   */
/* If vectOut != NULL, the newName argument is ignored.                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);
 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1,newName);}
 if(vect == NULL) return(NULL);

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++)
        {vect->data[i] = vect1->data[i] + vect2->data[i];}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = vect1->dataS[i] + vect2->dataS[i];}}
 else if(vect->type == FR_VECT_8R || vect->type == FR_VECT_16H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] + vect2->dataD[i];}}
 else if(vect->type == FR_VECT_4R || vect->type == FR_VECT_8H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] + vect2->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = vect1->dataI[i] + vect2->dataI[i];}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = vect1->dataL[i] + vect2->dataL[i];}}
 else if(vect->type == FR_VECT_8C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] + vect2->dataF[i];}}
 else if(vect->type == FR_VECT_16C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] + vect2->dataD[i];}}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUS[i] = vect1->dataUS[i] + vect2->dataUS[i];}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUI[i] = vect1->dataUI[i] + vect2->dataUI[i];}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUL[i] = vect1->dataUL[i] + vect2->dataUL[i];}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataU[i] = vect1->dataU[i] + vect2->dataU[i];}}

 return(vect);
}

/*------------------------------------------------------------------FrvBias--*/
FrVect *FrvBias(FrVect *vect1, double bias, FrVect *vectOut)
/*---------------------------------------------------------------------------*/
/* This function adds bias to vector vect1 .                                 */
/* If vectOut==NULL, vect1 is modified.  Otherwise, a new vector with name   */
/* newName is created.                                                       */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;

 if(vect1 == NULL) return(NULL);

 if(vectOut != NULL)
      {if(vectOut->type  != vect1->type)  return(NULL);
       if(vectOut->nData != vect1->nData) return(NULL);
       vect = vectOut;}
 else vect=vect1;
 /* output data type not supported! */
 if ((vect->type == FR_VECT_C) ||
     (vect->type == FR_VECT_8U)||
     (vect->type == FR_VECT_4U)||
     (vect->type == FR_VECT_2U)||
     (vect->type == FR_VECT_1U)) return (NULL);


 if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = vect1->dataS[i] + bias;}}
 else if(vect->type == FR_VECT_8R || vect->type == FR_VECT_16H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i]+ bias;}}
 else if(vect->type == FR_VECT_4R || vect->type == FR_VECT_8H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] + bias;}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = vect1->dataI[i]+ bias;}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = vect1->dataL[i] + bias;}}
 else if(vect->type == FR_VECT_8C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] + bias;}}
 else if(vect->type == FR_VECT_16C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] + bias;}}
 

 return(vect);
}

/*-------------------------------------------------------------FrvCombine2--*/
FrVect *FrvCombine2(double s1,
                    FrVect *vect1,
                    double s2,
                    FrVect *vect2,
                    FrVect *vect3,
                    char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vect3 = s1*vect1+ s2*vect2 (elements by elements). */
/* The vector vect3 could be vect1 or vect2. If vect3 = NULL, a new one      */
/* is created. Its name is then newName. It returns the vector result or NULL*/
/* in case of error. If vect3 != NULL, the newName argument is ignored.      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);
 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1,newName);}
 if(vect == NULL) return(NULL);

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++)
        {vect->data[i] = s1*vect1->data[i] + s2*vect2->data[i];}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = s1*vect1->dataS[i] + s2*vect2->dataS[i];}}
 else if(vect->type == FR_VECT_8R || vect->type == FR_VECT_16H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = s1*vect1->dataD[i] + s2*vect2->dataD[i];}}
 else if(vect->type == FR_VECT_4R || vect->type == FR_VECT_8H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = s1*vect1->dataF[i] + s2*vect2->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = s1*vect1->dataI[i] + s2*vect2->dataI[i];}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = s1*vect1->dataL[i] + s2*vect2->dataL[i];}}
 else if(vect->type == FR_VECT_8C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = s1*vect1->dataF[i] + s2*vect2->dataF[i];}}
 else if(vect->type == FR_VECT_16C)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = s1*vect1->dataD[i] + s2*vect2->dataD[i];}}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUS[i] = s1*vect1->dataUS[i] + s2*vect2->dataUS[i];}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUI[i] = s1*vect1->dataUI[i] + s2*vect2->dataUI[i];}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUL[i] = s1*vect1->dataUL[i] + s2*vect2->dataUL[i];}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataU[i] = s1*vect1->dataU[i] + s2*vect2->dataU[i];}}

 return(vect);
}

/*---------------------------------------------------------------FrvCombine--*/
FrVect *FrvCombine(int nVect,
                      ...)
/*---------------------------------------------------------------------------*/
/* Syntax: FrVect* = FrvCombine(int nVector, scale1, vect1, ...              */
/*                      ,scalen, vectn, FrVect* vectOut, char* newName);     */
/* This function computes vectOut = scale1*vect1+ ... up to n vectors        */
/* The vector vectOut could be any of the input vector. If vectOut = NULL,   */
/* a new one is created. Its name is newName.                                */
/* If vectOut != NULL, the newName argument is ignored.                      */
/*---------------------------------------------------------------------------*/
{va_list ap;
 FrVect *vect, **vectIn;
 double *scale;
 long i;
 char *name;

 if(nVect == 0) return(NULL);

 vectIn = (FrVect **) malloc(nVect * sizeof(FrVect*));
 scale  = (double *)  malloc(nVect * sizeof(double));
 if(scale == NULL || vectIn == NULL) return(NULL);

                    /*---------------- extract parameters -------------------*/

 va_start(ap, nVect);
 for(i=0; i<nVect; i++)
    {scale[i]  = va_arg(ap, double);
     vectIn[i] = va_arg(ap, FrVect*);}
 vect = va_arg(ap, FrVect*);
 name = va_arg(ap, char*);

                   /*--------------- perform operations --------------------*/
  
 if(nVect == 1)
   {vect = FrvScale(scale[i], vectIn[i], vect, name);}
else
   {vect = FrvCombine2(scale[0], vectIn[0], scale[0], vectIn[0], vect, name);
    if(nVect > 2)
    for(i=2; i<nVect; i++)
       {vect = FrvCombine2(1., vect, scale[i], vectIn[i], vect, NULL);}}

                   /*----------------- free local space----------------------*/

 free(vectIn);
 free(scale);

 return(vect);}

/*-----------------------------------------------------------------FrvDelta--*/
int FrvDelta(FrVect *vect, double *delta, double *previous)
/*---------------------------------------------------------------------------*/
/* This function computes maximum value of the differentiate vector:         */
/*       delta = max(abs(data[i+1] - data[i])).                              */
/* The result is return in delta. If previous != NULL, the corresponding     */
/* value is used to differentiate the first vector element. On return,       */
/* previous holds the value of the last vector element.                      */
/* This function returns a non zero value in case of error.                  */
/*---------------------------------------------------------------------------*/
{int i;
 double prev, val, diff;

 if(vect == NULL)     return(1);
 if(vect->nData == 0) return(1);

 *delta= 0.;
 val = 0;

 if(vect->type == FR_VECT_C)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->data[0];}
     for(i=0; i<vect->nData; i++) {val = vect->data[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_2S)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataS[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataS[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_8R)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataD[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataD[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_4R)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataF[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataF[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_4S)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataI[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataI[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_8S)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataL[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataL[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_8C)
    {return(0);}
 else if(vect->type == FR_VECT_16C)
    {return(0);}
 else if(vect->type == FR_VECT_2U)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataUS[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataUS[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_4U)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataUI[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataUI[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_8U)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataUL[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataUL[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else if(vect->type == FR_VECT_1U)
    {if(previous != NULL) 
          {prev = *previous;}
     else {prev = vect->dataU[0];}
     for(i=0; i<vect->nData; i++) {val = vect->dataU[i];
                                   diff = val - prev;
                                   if(diff < 0) diff = -diff;
				   if(diff > *delta) *delta = diff;
                                   prev = val;}}
 else{return(2);}
    
 if(previous != NULL) *previous = val;

 return(0);}

/*----------------------------------------------------------------FrvDivide--*/
FrVect *FrvDivide(FrVect *vect1,
                  FrVect *vect2,
                  FrVect *vect3,
                  char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vectOut= vect1/vect2 elements by elements after    */
/* checking the division by 0. ( if the denominator is zero then the result  */
/* is set also to 0. The vector vectOut could be vect1 or vect2.             */
/* If vectOut = NULL, a new one is created called newName.                   */
/* This function returns, the vector result or NULL in case of error.        */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;
 float tempF;
 double tempD, denom;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);
 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1, newName);}
 if(vect == NULL) return(NULL);

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->data[i] == 0.) 
              vect->data[i] = 0.;
         else vect->data[i] = vect1->data[i] / vect2->data[i];}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataS[i] == 0.) 
              vect->dataS[i] = 0.;
         else vect->dataS[i] = vect1->dataS[i] / vect2->dataS[i];}}
 else if(vect->type == FR_VECT_8R)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataD[i] == 0.) 
              vect->dataD[i] = 0.;
         else vect->dataD[i] = vect1->dataD[i] / vect2->dataD[i];}}
 else if(vect->type == FR_VECT_4R)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataF[i] == 0.) 
              vect->dataF[i] = 0.;
         else vect->dataF[i] = vect1->dataF[i] / vect2->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataI[i] == 0.) 
              vect->dataI[i] = 0.;
         else vect->dataI[i] = vect1->dataI[i] / vect2->dataI[i];}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataL[i] == 0.) 
              vect->dataL[i] = 0.;
         else vect->dataL[i] = vect1->dataL[i] / vect2->dataL[i];}}
 else if(vect->type == FR_VECT_8C)
    {for(i=0; i<2*vect->nData; i=i+2)
        {denom = vect2->dataF[i]   * vect2->dataF[i] +
	         vect2->dataF[i+1] * vect2->dataF[i+1];
	 if(denom == 0)
	    {vect->dataF[i  ] = 0.;
             vect->dataF[i+1] = 0.;}
         else
            {tempF = (vect1->dataF[i]   * vect2->dataF[i] +
                      vect1->dataF[i+1] * vect2->dataF[i+1])/denom;
             vect->dataF[i+1] = (- vect1->dataF[i]   * vect2->dataF[i+1] +
                                   vect1->dataF[i+1] * vect2->dataF[i])/denom;
             vect->dataF[i] = tempF;}}}
 else if(vect->type == FR_VECT_16C)
    {for(i=0; i<2*vect->nData; i=i+2)
        {denom = vect2->dataD[i]   * vect2->dataD[i] +
	         vect2->dataD[i+1] * vect2->dataD[i+1];
	 if(denom == 0)
	    {vect->dataD[i  ] = 0.;
             vect->dataD[i+1] = 0.;}
         else
            {tempD = (vect1->dataD[i]   * vect2->dataD[i] +
                      vect1->dataD[i+1] * vect2->dataD[i+1])/denom;
             vect->dataD[i+1] = (- vect1->dataD[i]   * vect2->dataD[i+1] +
                                   vect1->dataD[i+1] * vect2->dataD[i])/denom;
             vect->dataD[i] = tempD;}}}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataUS[i] == 0.) 
              vect->dataUS[i] = 0.;
         else vect->dataUS[i] = vect1->dataUS[i] / vect2->dataUS[i];}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataUI[i] == 0.) 
              vect->dataUI[i] = 0.;
         else vect->dataUI[i] = vect1->dataUI[i] / vect2->dataUI[i];}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataUL[i] == 0.) 
              vect->dataUL[i] = 0.;
         else vect->dataUL[i] = vect1->dataUL[i] / vect2->dataUL[i];}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++)
        {if(vect2->dataU[i] == 0.) 
              vect->dataU[i] = 0.;
         else vect->dataU[i] = vect1->dataU[i] / vect2->dataU[i];}}

 return(vect);
}
/*---------------------------------------------------------------FrvFlatten--*/
FrVect *FrvFlatten(FrVect *vect1, FrVect *vectOut)
/*---------------------------------------------------------------------------*/
/*This function puts vector extrema equal to zero, subtracting a straigth    */
/*line.                                                                      */
/*If vectOut==NULL, vect1 is modified. Otherwise, a new vector with name     */
/*newName is created.                                                        */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;
 double a, b;
 if(vect1 == NULL) return(NULL);


 if(vectOut != NULL)
      {if(vectOut->type  != vect1->type)  return(NULL);
       if(vectOut->nData != vect1->nData) return(NULL);
       vect = vectOut;}
 else vect=vect1;
 /* output data type not supported! */
 if ((vect->type == FR_VECT_C) ||
     (vect->type == FR_VECT_8U)||
     (vect->type == FR_VECT_4U)||
     (vect->type == FR_VECT_2U)||
     (vect->type == FR_VECT_1U)) return (NULL);


 else if(vect->type == FR_VECT_2S)
   {b=vect1->dataS[0];
   a=(vect1->dataS[vect1->nData-1]-vect1->dataS[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = vect1->dataS[i] - b-a*i;}}

 else if(vect->type == FR_VECT_8R)
   {b=vect1->dataD[0];
   a=(vect1->dataD[vect1->nData-1]-vect1->dataD[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] - b-a*i;}}

 else if(vect->type == FR_VECT_4R)
   {b=vect1->dataF[0];
   a=(vect1->dataF[vect1->nData-1]-vect1->dataF[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - b-a*i;}}

 else if(vect->type == FR_VECT_4S)
   {b=vect1->dataI[0];
   a=(vect1->dataI[vect1->nData-1]-vect1->dataI[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = vect1->dataI[i] - b-a*i;}}

 else if(vect->type == FR_VECT_8S)
   {b=vect1->dataL[0];
   a=(vect1->dataL[vect1->nData-1]-vect1->dataL[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = vect1->dataL[i] - b-a*i;}}

 else if(vect->type == FR_VECT_8C)
   {b=vect1->dataF[0];
   a=(vect1->dataF[vect1->nData-1]-vect1->dataF[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - b-a*i;}}

 else if(vect->type == FR_VECT_16C)
   {b=vect1->dataD[0];
   a=(vect1->dataD[vect1->nData-1]-vect1->dataD[0])/(vect1->nData-1);
      for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] - b-a*i;}}

 /* aggiungere il controllo sugli unsigned! */
 return(vect);
}
/*-------------------------------------------------------------FrvIntegrate--*/
FrVect* FrvIntegrate(FrVect *inVect, int n, char* newName)
/*---------------------------------------------------------------------------*/
/* This function performs the "n"-order Integration (or Derivation if n<0)   */
/* of a signal in the frequency domain.                                      */
/* It takes as input a FrVect vector structure "inVect"                      */
/* and DIVIDES it by omega^n                                                 */
/* A new vector of type FR_VECT_8R (double) is created with name "newName".  */
/* Allowed "n" values: n=0 (trivial case: no operation is performed          */
/*                     n>0 input spectrum is INTEGRATED n times              */
/*                     n<0 input spectrum is DERIVED n times                 */
/*                         (i.e. it is MULTIPLIED by omega^abs(n))           */
/* This function returns the resulting vector or NULL in case of error.      */
/*---------------------------------------------------------------------------*/
{
 FrVect* outVect;
 int i,ii;
 char old_unitY[128], str1[64],str2[64],patch[64], * p;
 double dx, startX;

 if(inVect == NULL) return(NULL);
 if(n==0) return(NULL);

 outVect = FrvCopyToD(inVect,1.,newName);

 if(outVect == NULL) {
   printf("<FrvIntegrate> Error creating outVect\n"); 
   return(NULL);
 }
                            /*--------------------- sets the new Y units ---*/  

 if(outVect->unitY != NULL) {
   sscanf(outVect->unitY,"%s",old_unitY);
   p = strtok(old_unitY,"/");
   if(p != NULL) {
      strcpy(str1,p);
      p = strtok('\0'," ");
      if(p != NULL) {
         strcpy(str2,p);
         sprintf(patch,"s%+d/",n);
         strcat(str1,patch);
         strcat(str1,str2);
         FrStrCpy(&outVect->unitY,str1);
      }}}
                           /*--------------------Integrate (or Derivate) ---*/
 
 dx     = FRVTWOPI*inVect->dx[0];
 startX = FRVTWOPI*inVect->startX[0];
 if(startX == 0) startX = 0.5*dx;

 for(i=1;i<=abs(n);i++)
   for(ii=0;ii<outVect->nData;ii++) 
     {
       if(n>0) outVect->dataD[ii] = outVect->dataD[ii] / (ii*dx+startX);
       else    outVect->dataD[ii] = outVect->dataD[ii] * (ii*dx+startX);
     }

 return(outVect);
}
/*----------------------------------------------------------FrvInterpolate---*/
FrVect *FrvInterpolate(FrVect *vIn,
                       FrVect *vOut,
                       int n,
                       int iStart,
                       int nIn,
                       char *option)
/*---------------------------------------------------------------------------*/
/* Creates an interpolated vector from (part of) initial vector vIn
   Takes vIn elements from iStart to istart+nIn-1
   Size of output vector is n*(nIn-1)+1
   Option selects interpolation method: default is "Q" for quadratic interp.
   Supported types for vIn are double and float                              */
/*---------------------------------------------------------------------------*/
{int i, j, k, nDataOut;
 double a=0, b=0, c=0, dx, step;

 if(vIn == NULL) return(NULL);
 if(n   == 0)    return(NULL);
 if(iStart > vIn->nData) iStart = 0;
 if(iStart + nIn > vIn->nData) nIn = vIn->nData - iStart;
 
                                 /*----- create output vector if needed ---*/
 nDataOut = n*(nIn-1)+1;
 if(vOut == NULL)
   {dx    = vIn->dx[0] / (double)n;
    vOut = FrVectNew1D(vIn->name,-vIn->type, nDataOut, dx, 
                                  vIn->unitX[0], vIn->unitY);
    if(vOut == NULL) return(NULL);}
 else
   {if(vOut->nData != nDataOut)  return(NULL);
    if(vOut->type  != vIn->type) return(NULL);}
    
                                   /*------------ transfer timing info ---*/
 vOut->startX[0] = vIn->startX[0];
 vOut->GTime     = vIn->GTime + ((double)iStart*vIn->dx[0]);

                                  /*---------------- do interpolation ----*/

 k = 0;
 step = 1./(double)n;

 if(vIn->type == FR_VECT_8R)
   {for(i=iStart; i<iStart+nIn-1; i++)
      {if(i < vIn->nData-2)
          {c =  vIn->dataD[i];
           a = (vIn->dataD[i+2] - 2*vIn->dataD[i+1] + c)*.5;
           b =  vIn->dataD[i+1] - a - c;
           a = a*step*step;
	   b = b*step;}
       else
          {c = a*n*n+b*n+c;
           b = b+2*a*n;}
       for(j=0; j<n; j++)
          {vOut->dataD[k] = (a*j+b)*j + c;
	   k++;}}
    vOut->dataD[k] = vIn->dataD[i];}

 else if(vIn->type == FR_VECT_4R)
   {for(i=iStart; i<iStart+nIn-1; i++)
      {if(i < vIn->nData-2)
          {c =  vIn->dataF[i];
           a = (vIn->dataF[i+2] - 2*vIn->dataF[i+1] + c)*.5;
           b =  vIn->dataF[i+1] - a - c;
           a = a*step*step;
	   b = b*step;}
       else
          {c = a*n*n+b*n+c;
           b = b+2*a*n;}
       for(j=0; j<n; j++)
          {vOut->dataF[k] = (a*j+b)*j + c;
	   k++;}}
    vOut->dataF[k] = vIn->dataF[i];}
    
 else {return(NULL);}
  	 
 return(vOut);
}
/*----------------------------------------------------------FrvInterpolate---*/
FrVect *FrvInterpolatePhase(FrVect *vIn,
			    FrVect *vOut,
			    int n,
			    int iStart,
			    int nIn,
			    char *option)
/*---------------------------------------------------------------------------*/
/* Creates an interpolated vector from (part of) initial vector vIn
   Takes vIn elements from iStart to istart+nIn-1
   Size of output vector is n*nIn
   Option selects interpolation method: default is "L" for linear interpolation
   Supported types for vIn are double and float
*/
/*---------------------------------------------------------------------------*/
{int i, j, k, nDataOut;
 double dPhase, dx, step;

 if(vIn == NULL) return(NULL);
 if(n   == 0)    return(NULL);
 if(iStart > vIn->nData) iStart = 0;
 if(iStart + nIn > vIn->nData) nIn = vIn->nData - iStart;
 
                                 /*----- create output vector if needed ---*/
 nDataOut = n*(nIn-1)+1;
 if(vOut == NULL)
   {dx    = vIn->dx[0] / (double)n;
    vOut = FrVectNew1D(vIn->name,-vIn->type, nDataOut, dx, 
                                  vIn->unitX[0], vIn->unitY);
    if(vOut == NULL) return(NULL);}
 else
   {if(vOut->nData != nDataOut)  return(NULL);
    if(vOut->type  != vIn->type) return(NULL);}
    
                                   /*------------ transfer timing info ---*/
 vOut->startX[0] = vIn->startX[0];
 vOut->GTime     = vIn->GTime + ((double)iStart*vIn->dx[0]);

                                  /*-----------------do interpolation ----*/
 k = 0;
 step = 1./(double)n;

 if(vIn->type == FR_VECT_8R)
   {dPhase = 0.;
    for(i=iStart; i<iStart+nIn-1; i++)
     {dPhase = vIn->dataD[i+1] - vIn->dataD[i];
      if(dPhase >  3.14) dPhase = dPhase - FRVTWOPI;
      if(dPhase < -3.14) dPhase = dPhase + FRVTWOPI;
      dPhase = dPhase*step;

      vOut->dataD[k] = vIn->dataD[i];
      k++;
      for(j=1; j<n; j++)
         {vOut->dataD[k] = vOut->dataD[k-1] + dPhase;
	 k++;}}
    vOut->dataD[k] =  vOut->dataD[k-1] + dPhase;}
    
 else if(vIn->type == FR_VECT_4R)
   {dPhase = 0.;
    for(i=iStart; i<iStart+nIn-1; i++)
     {dPhase = vIn->dataF[i+1] - vIn->dataF[i];
      if(dPhase >  3.14) dPhase = dPhase - FRVTWOPI;
      if(dPhase < -3.14) dPhase = dPhase + FRVTWOPI;
      dPhase = dPhase*step;

      vOut->dataF[k] = vIn->dataF[i];
      k++;
      for(j=1; j<n; j++)
         {vOut->dataF[k] = vOut->dataF[k-1] + dPhase;
	 k++;}}
    vOut->dataF[k] =  vOut->dataF[k-1] + dPhase;}
    
 else {return(NULL);}
  	 
 return(vOut);
}
/*---------------------------------------------------------------FrvModulus--*/
FrVect *FrvModulus(FrVect *vectIn,
                   FrVect *vectOut,
                   char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes the modulus of a complex vector vectIn.            */
/* The result is stored in vectOut, a real vector (Float or Double according */
/* to the complex type of vectIn). If vectOut = NULL, a new vector is created*/
/* Its name is newName or 'module(vectIn->name)' if newName = NULL.          */
/* It returns, the vector result or NULL in case of error.                   */
/*---------------------------------------------------------------------------*/
{char *name = NULL;
 double *dataD;
 float *dataF;
 int i, nData;

 if(vectIn == NULL) return(NULL);
 dataF = vectIn->dataF;
 dataD = vectIn->dataD;
 nData = vectIn->nData;
 
                               /*---- create the output vector if needed ---*/
 if(vectOut == NULL)
   {if(newName == NULL) 
      {name = malloc(strlen(vectIn->name)+10);
       if(name == NULL) return(NULL);
       sprintf(name,"modulus(%s)",vectIn->name);
       newName = name;}

    if(vectIn->type == FR_VECT_C8)
        {vectOut = FrVectNew1D(newName, FR_VECT_4R, nData, 
                   vectIn->dx[0], vectIn->unitX[0], vectIn->unitY);}
    else if(vectIn->type == FR_VECT_C16)
        {vectOut = FrVectNew1D(newName, FR_VECT_8R, nData, 
                   vectIn->dx[0], vectIn->unitX[0], vectIn->unitY);}
    else if(vectIn->type == FR_VECT_8H)
        {vectOut = FrVectNew1D(newName, FR_VECT_4R, nData/2, 
                   vectIn->dx[0], vectIn->unitX[0], vectIn->unitY);}
    else if(vectIn->type == FR_VECT_16H)
        {vectOut = FrVectNew1D(newName, FR_VECT_8R, nData/2, 
                   vectIn->dx[0], vectIn->unitX[0], vectIn->unitY);}
    
    if(vectOut == NULL) return(NULL);
    vectOut->startX[0] = vectIn->startX[0];}
    
                                 /*----------------- compute the modulus ---*/
				 

  if(vectIn->type == FR_VECT_C8)
    {if(vectOut->type != FR_VECT_4R) return(NULL);
     if(vectOut->nData != nData)     return(NULL);
     for(i=0; i<nData; i++)
        {vectOut->dataF[i] = sqrt(dataF[2*i+1]*dataF[2*i+1] +
	                          dataF[2*i]  *dataF[2*i]);}}
	
 else if(vectIn->type == FR_VECT_C16)
    {if(vectOut->type != FR_VECT_8R) return(NULL);
     if(vectOut->nData != nData)     return(NULL);
     for(i=0; i<nData; i++)
        {vectOut->dataD[i] = sqrt(dataD[2*i+1]*dataD[2*i+1] +
	                          dataD[2*i]  *dataD[2*i]);}}
	
 else if(vectIn->type == FR_VECT_8H)
    {if(vectOut->type != FR_VECT_4R) return(NULL);
     if(vectOut->nData*2 != nData)   return(NULL);
     vectOut->dataF[0] = dataD[0];
     for(i=1; i<nData/2; i++)
        {vectOut->dataF[i] = sqrt(dataF[i]       *dataF[i]+
	                          dataF[nData-i]*dataF[nData-i]);}}
	
 else if(vectIn->type == FR_VECT_16H)
    {if(vectOut->type != FR_VECT_8R) return(NULL);
     if(vectOut->nData*2 != nData)   return(NULL);
     vectOut->dataD[0] = dataD[0];
     for(i=1; i<nData/2; i++)
        {vectOut->dataD[i] = sqrt(dataD[i]       *dataD[i] +
	                          dataD[nData-i]*dataD[nData-i]);}}
	
 else{return(NULL);}
 
 if(name != NULL) free(name);

 return(vectOut);
}

/*------------------------------------------------------------------FrvMult--*/
FrVect *FrvMult(FrVect *vect1,
                FrVect *vect2,
                FrVect *vect3,
                char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vect3= vect1*vect2 elements by elements.           */
/* The vector vect3 could be vect1 or vect2. If vect3 = NULL a new vector    */
/* is created. Its name is then newName.                                     */
/* This function returns, the vector result or NULL in case of error.        */
/* If vectOut = NULL, the newName argument is ignored.                       */
/*---------------------------------------------------------------------------*/
{int i, nData, nDataH, real, imag;
 double *v1D, *v2D, *vOutD, tempD;
 float  *v1F, *v2F, *vOutF, tempF;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);}
 else {vect3 = FrvClone(vect1, newName);}
 if(vect3 == NULL) return(NULL);

 nData  = vect1->nData;
 nDataH = vect1->nData/2;

                         /*------------------------ first a special case ----*/

 if((vect1->type == FR_VECT_H8) &&
    (vect2->type == FR_VECT_8R))
   {if(vect1->nData/2 != vect2->nData-1)   return(NULL);
    v1F    = vect1->dataF;
    v2D    = vect2->dataD;
    vOutF  = vect3->dataF;
    for (i=0; i<nDataH-1; i++) 
       {real = i+1;
        imag = nData-i-1;
        vOutF[real] = v1F[real]*v2D[real];
        vOutF[imag] = v1F[imag]*v2D[real];}
      
    vOutF[0]      = v1F[0]     *v2D[0];
    vOutF[nDataH] = v1F[nDataH]*v2D[nDataH];
    return(vect3);}

 if((vect1->type == FR_VECT_H16) &&
    (vect2->type == FR_VECT_8R))
   {if(vect1->nData/2 != vect2->nData-1)   return(NULL);
    v1D    = vect1->dataD;
    v2D    = vect2->dataD;
    vOutD  = vect3->dataD;
    for (i=0; i<nDataH-1; i++) 
       {real = i+1;
        imag = nData-i-1;
        vOutD[real] = v1D[real]*v2D[real];
        vOutD[imag] = v1D[imag]*v2D[real];}
      
    vOutD[0]      = v1D[0]     *v2D[0];
    vOutD[nDataH] = v1D[nDataH]*v2D[nDataH];
    return(vect3);}

                     /*-----  now this is the case of vector of same type ---*/

 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if((vect1->type == FR_VECT_H16) &&
    (vect2->type == FR_VECT_H16))
   {v1D    = vect1->dataD;
    v2D    = vect2->dataD;
    vOutD  = vect3->dataD;
    for (i=0; i<nDataH-1; i++) 
       {real = i+1;
        imag = nData-i-1;
        tempD       = v1D[real]*v2D[real] - v1D[imag]*v2D[imag];
        vOutD[imag] = v1D[real]*v2D[imag] + v1D[imag]*v2D[real];
        vOutD[real] = tempD;}
      
    vOutD[0]      = v1D[0]     *v2D[0];
    vOutD[nDataH] = v1D[nDataH]*v2D[nDataH];}

 else if((vect1->type == FR_VECT_H8) &&
         (vect2->type == FR_VECT_H8))
   {v1F    = vect1->dataF;
    v2F    = vect2->dataF;
    vOutF  = vect3->dataF;
    for (i=0; i<nDataH-1; i++) 
       {real = i+1;
        imag = nData-i-1;
        tempF       = v1F[real]*v2F[real] - v1F[imag]*v2F[imag];
        vOutF[imag] = v1F[real]*v2F[imag] + v1F[imag]*v2F[real];
        vOutF[real] = tempF;}
      
    vOutF[0]      = v1F[0]     *v2F[0];
    vOutF[nDataH] = v1F[nDataH]*v2F[nDataH];}

 else if(vect3->type == FR_VECT_C)
    {for(i=0; i<vect3->nData; i++)
        {vect3->data[i] = vect1->data[i] * vect2->data[i];}}
 else if(vect3->type == FR_VECT_2S)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataS[i] = vect1->dataS[i] * vect2->dataS[i];}}
 else if(vect3->type == FR_VECT_8R)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataD[i] = vect1->dataD[i] * vect2->dataD[i];}}
 else if(vect3->type == FR_VECT_4R)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataF[i] = vect1->dataF[i] * vect2->dataF[i];}}
 else if(vect3->type == FR_VECT_4S)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataI[i] = vect1->dataI[i] * vect2->dataI[i];}}
 else if(vect3->type == FR_VECT_8S)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataL[i] = vect1->dataL[i] * vect2->dataL[i];}}
 else if(vect3->type == FR_VECT_8C)
    {for(i=0; i<2*vect3->nData; i=i+2)
        {tempF = vect1->dataF[i]   * vect2->dataF[i] -
                 vect1->dataF[i+1] * vect2->dataF[i+1];
         vect3->dataF[i+1] = vect1->dataF[i]   * vect2->dataF[i+1] +
                             vect1->dataF[i+1] * vect2->dataF[i];
         vect3->dataF[i] = tempF;}}
 else if(vect3->type == FR_VECT_16C)
    {for(i=0; i<2*vect3->nData; i=i+2)
        {tempD = vect1->dataD[i]   * vect2->dataD[i] -
                 vect1->dataD[i+1] * vect2->dataD[i+1];
         vect3->dataD[i+1] = vect1->dataD[i]   * vect2->dataD[i+1] +
                             vect1->dataD[i+1] * vect2->dataD[i];
         vect3->dataD[i] = tempD;}}
 else if(vect3->type == FR_VECT_2U)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataUS[i] = vect1->dataUS[i] * vect2->dataUS[i];}}
 else if(vect3->type == FR_VECT_4U)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataUI[i] = vect1->dataUI[i] * vect2->dataUI[i];}}
 else if(vect3->type == FR_VECT_8U)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataUL[i] = vect1->dataUL[i] * vect2->dataUL[i];}}
 else if(vect3->type == FR_VECT_1U)
    {for(i=0; i<vect3->nData; i++)
        {vect3->dataU[i] = vect1->dataU[i] * vect2->dataU[i];}}

 return(vect3);
}
/*--------------------------------------------------------------FrvMultConj--*/
FrVect *FrvMultConj(FrVect *vect1,
                    FrVect *vect2,
                    FrVect *vect3,
                    char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vect3= vect1*cc(vect2) elements by elements.       */
/* The vector vect3 could be vect1 or vect2. If vect3 = NULL a new vector    */
/* is created. Its name is then newName.                                     */
/* This function returns, the vector result or NULL in case of error.        */
/* If vectOut = NULL, the newName argument is ignored.                       */
/* Supported types are FR_VECT_C8 and FR_VECT_C16.                           */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;
 float tempF;
 double tempD;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);
 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1, newName);}
 if(vect == NULL) return(NULL);

 if(vect->type == FR_VECT_8C)
    {for(i=0; i<2*vect->nData; i=i+2)
        {tempF = vect1->dataF[i]   * vect2->dataF[i] +
                 vect1->dataF[i+1] * vect2->dataF[i+1];
         vect->dataF[i+1] = - vect1->dataF[i]   * vect2->dataF[i+1] +
                              vect1->dataF[i+1] * vect2->dataF[i];
         vect->dataF[i] = tempF;}}
 else if(vect->type == FR_VECT_16C)
    {for(i=0; i<2*vect->nData; i=i+2)
        {tempD = vect1->dataD[i]   * vect2->dataD[i] +
                 vect1->dataD[i+1] * vect2->dataD[i+1];
         vect->dataD[i+1] = - vect1->dataD[i]   * vect2->dataD[i+1] +
                              vect1->dataD[i+1] * vect2->dataD[i];
         vect->dataD[i] = tempD;}}
 else 
   {if(vect3 == NULL) free(vect);
    return(NULL);}

 return(vect);
}

/*-----------------------------------------------------------------FrvPhase--*/
FrVect *FrvPhase(FrVect *vectIn,
                 FrVect *vectOut,
                 char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes the phase of a complex vector vectIn.              */
/* The result is stored in vectOut, a real vector (Float or Double according */
/* to the complex type of vectIn). If vectOut = NULL, a new one is created.  */
/* Its name is newName or "phase(vectIn->name)" if newName = NULL.           */
/* It returns, the vector result or NULL in case of error.                   */
/*---------------------------------------------------------------------------*/
{char *name = NULL;
 double *dataD;
 float *dataF;
 int i, nData;

 if(vectIn == NULL) return(NULL);
 dataF = vectIn->dataF;
 dataD = vectIn->dataD;
 nData = vectIn->nData;
 
                               /*---- create the output vector if needed ---*/
 if(vectOut == NULL)
   {if(newName == NULL) 
      {name = malloc(strlen(vectIn->name)+10);
       if(name == NULL) return(NULL);
       sprintf(name,"phase(%s)",vectIn->name);
       newName = name;}

    if(vectIn->type == FR_VECT_C8)
        {vectOut = FrVectNew1D(newName, FR_VECT_4R, nData, 
                   vectIn->dx[0], vectIn->unitX[0], "[radian]");}
    else if(vectIn->type == FR_VECT_C16)
        {vectOut = FrVectNew1D(newName, FR_VECT_8R, nData, 
                   vectIn->dx[0], vectIn->unitX[0], "[radian]");}
    else if(vectIn->type == FR_VECT_8H)
        {vectOut = FrVectNew1D(newName, FR_VECT_4R, nData/2, 
                   vectIn->dx[0], vectIn->unitX[0], "[radian]");}
    else if(vectIn->type == FR_VECT_16H)
        {vectOut = FrVectNew1D(newName, FR_VECT_8R, nData/2, 
                   vectIn->dx[0], vectIn->unitX[0], "[radian]");}
    
    if(vectOut == NULL) return(NULL);
    vectOut->startX[0] = vectIn->startX[0];}
    
                                 /*----------------- compute the phase ---*/
				 

  if(vectIn->type == FR_VECT_C8)
    {if(vectOut->type != FR_VECT_4R) return(NULL);
     if(vectOut->nData != nData)     return(NULL);
     for(i=0; i<nData; i++)
        {vectOut->dataF[i] = atan2(dataF[2*i+1],dataF[2*i]);}}
	
 else if(vectIn->type == FR_VECT_C16)
    {if(vectOut->type != FR_VECT_8R) return(NULL);
     if(vectOut->nData != nData)     return(NULL);
     for(i=0; i<nData; i++)
        {vectOut->dataD[i] = atan2(dataD[2*i+1],dataD[2*i]);}}
	
 else if(vectIn->type == FR_VECT_8H)
    {if(vectOut->type != FR_VECT_4R) return(NULL);
     if(vectOut->nData*2 != nData)   return(NULL);
     vectOut->dataF[0] = atan2(0,dataD[0]);;
     for(i=1; i<nData/2; i++)
        {vectOut->dataF[i] = atan2(dataF[nData-i],dataF[i]);}}
	
 else if(vectIn->type == FR_VECT_16H)
    {if(vectOut->type != FR_VECT_8R) return(NULL);
     if(vectOut->nData*2 != nData)   return(NULL);
     vectOut->dataD[0] = atan2(0,dataD[0]);
     for(i=1; i<nData/2; i++)
        {vectOut->dataD[i] = atan2(dataD[nData-i],dataD[i]);}}
	
 else{return(NULL);}
 
 if(name != NULL) free(name);

 return(vectOut);
}
/*-------------------------------------------------------------------FrvRms--*/
int FrvRms(FrVect *vect, double *mean, double *rms)
/*---------------------------------------------------------------------------*/
/* This function computes the rms and mean value of the input vector vect.   */
/* The result is put in mean and rms. It returns 0 in case of success.       */
/*---------------------------------------------------------------------------*/
{int i;
 double sum, sum2, val, rms2;

 if(vect == NULL)     return(1);
 if(vect->nData == 0) return(2);

 sum  = 0.;
 sum2 = 0.;

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++) {val = vect->data[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++) {val = vect->dataS[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_8R)
    {for(i=0; i<vect->nData; i++) {val = vect->dataD[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_4R)
    {for(i=0; i<vect->nData; i++) {val = vect->dataF[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++) {val = vect->dataI[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++) {val = vect->dataL[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_C8)
    {return(3);}
 else if(vect->type == FR_VECT_C16)
    {return(3);}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++) {val = vect->dataUS[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++) {val = vect->dataUI[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++) {val = vect->dataUL[i];
                                   sum  += val;
                                   sum2 += val*val;}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++) {val = vect->dataU[i];
                                   sum  += val;
                                   sum2 += val*val;}}

 *mean = sum/vect->nData;
 rms2 = sum2/vect->nData - (*mean)*(*mean);
 *rms = sqrt(rms2);

 return(0);}

/*-----------------------------------------------------------------FrvScale--*/
FrVect *FrvScale(double scale,
                 FrVect *vect1,
                 FrVect *vect3,
                 char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vect3 = scale*vect1 elements by elements.          */
/* The vector vect3 could be vect1 or vect2.                                 */
/* If vect3 = NULL, a new one is created. Its name is then newName.          */
/* It returns, the vector result or NULL in case of error.                   */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i, scaleI;
 char   scaleC;
 short  scaleS;
 long   scaleL;
 float  scaleF;
 double scaleD;
 unsigned char  scaleU;
 unsigned short scaleUS;
 unsigned int   scaleUI;
 unsigned long  scaleUL;

 if(vect1 == NULL) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1,newName);}
 if(vect == NULL) return(NULL);

 if(vect->type == FR_VECT_C)
    {scaleC = scale;
     for(i=0; i<vect->nData; i++) 
        {vect->data[i] = scaleC * vect1->data[i];}}
 else if(vect->type == FR_VECT_2S)
    {scaleS = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = scaleS * vect1->dataS[i];}}
 else if(vect->type == FR_VECT_8R || vect->type == FR_VECT_16H)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = scale * vect1->dataD[i];}}
 else if(vect->type == FR_VECT_4R || vect->type == FR_VECT_8H)
    {scaleF = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = scaleF * vect1->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
    {scaleI = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = scaleI * vect1->dataI[i];}}
 else if(vect->type == FR_VECT_8S)
    {scaleL = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = scaleL * vect1->dataL[i];}}
 else if(vect->type == FR_VECT_8C)
    {scaleF = scale;
     for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = scaleF * vect1->dataF[i];}}
 else if(vect->type == FR_VECT_16C)
    {scaleD = scale;
     for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = scaleD * vect1->dataD[i];}}
 else if(vect->type == FR_VECT_2U)
    {scaleUS = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataUS[i] = scaleUS * vect1->dataUS[i];}}
 else if(vect->type == FR_VECT_4U)
    {scaleUI = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataUI[i] = scaleUI * vect1->dataUI[i];}}
 else if(vect->type == FR_VECT_8U)
    {scaleUL = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataUL[i] = scaleUL * vect1->dataUL[i];}}
 else if(vect->type == FR_VECT_1U)
    {scaleU = scale;
     for(i=0; i<vect->nData; i++)
        {vect->dataU[i] = scaleU * vect1->dataU[i];}}

 return(vect);
}

/*--------------------------------------------------------------FrvStatFree--*/
void FrvStatFree(FrvStat *stat)
/*---------------------------------------------------------------------------*/
{
 if(stat->mean != NULL) FrVectFree(stat->mean);
 if(stat->rms != NULL) FrVectFree(stat->rms);
 free(stat);

 return;
}
/*--------------------------------------------------------------FrvStatNew--*/
FrvStat* FrvStatNew()
/*---------------------------------------------------------------------------*/
{FrvStat *stat;

 stat = (FrvStat *) (calloc(1,sizeof(FrvStat)));
 if(stat == NULL) return(NULL);

 stat->decay = 1.;

 return(stat);
}
/*--------------------------------------------------------------FrvStatProc--*/
FrvStat* FrvStatProc(FrvStat *stat, FrVect *vect)
/*---------------------------------------------------------------------------*/
{double decay, input;
 int i;
 char name[256];

 if(vect == NULL)  return(stat);

 if(stat  == NULL)
   {stat = FrvStatNew();
   if(stat == NULL) return(stat);}

 if(stat->mean != NULL)
  {if(stat->mean->nData != vect->nData)
    {FrVectFree(stat->mean);
     FrVectFree(stat->rms);
     stat->mean = NULL;
     stat->rms  = NULL;}}

 if(stat->mean == NULL)
   {sprintf(name,"%s mean",vect->name);
   stat->mean  = FrVectNew1D(name, FR_VECT_8R, vect->nData,
                             vect->dx[0], "time [s]",vect->unitY);
   sprintf(name,"%s RMS",vect->name);
   stat->rms  = FrVectNew1D(name, FR_VECT_8R, vect->nData,
                            vect->dx[0], "time [s]",vect->unitY);
   if(stat->mean == NULL || stat->rms == NULL)
     {FrvStatFree(stat);
     return(NULL);}}

 stat->dt = vect->dx[0];
 if(stat->decay == -1.) stat->decay = 1.-stat->dt/stat->decayTime;

 input = 0.;
 for(i=0; i<vect->nData; i++)
   {if    (vect->type == FR_VECT_4R) input = vect->dataF[i];
   else if(vect->type == FR_VECT_8R) input = vect->dataD[i];
   else if(vect->type == FR_VECT_2S) input = vect->dataS[i];
   else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
   else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
   else if(vect->type == FR_VECT_C)  input = vect->data[i];
   else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
   else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
   else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
   else if(vect->type == FR_VECT_1U) input = vect->dataU[i];

   stat->nDecay++;
   if(stat->nDecay == 1)
     {stat->meanA = input;
     stat->x2A = input*input;}
   else
     {decay = 1.-1./stat->nDecay;
     if(stat->decay < decay) decay = stat->decay;
     stat->meanA =  decay*stat->meanA + (1.-decay)*input;
     stat->x2A = decay*stat->x2A + (1.-decay)*input*input;}
   stat->mean->dataD[i] =  stat->meanA;
   stat->rms->dataD[i] = sqrt(stat->x2A - stat->meanA*stat->meanA);}

 stat->rmsA = stat->rms->dataD[stat->rms->nData-1];

 return(stat);
}
/*--------------------------------------------------------------FrvStatProc--*/
FrvStat* FrvStatProcA(FrvStat *stat, FrVect *vect)
/*---------------------------------------------------------------------------*/
{double decay, input;
 int i;

 if(vect == NULL)  return(stat);

 if(stat  == NULL)
   {stat = FrvStatNew();
   if(stat == NULL) return(stat);}

 stat->dt = vect->dx[0];
 if(stat->decay == -1.) stat->decay = 1.-stat->dt/stat->decayTime;

 input = 0.;
 for(i=0; i<vect->nData; i++)
   {if    (vect->type == FR_VECT_4R) input = vect->dataF[i];
   else if(vect->type == FR_VECT_8R) input = vect->dataD[i];
   else if(vect->type == FR_VECT_2S) input = vect->dataS[i];
   else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
   else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
   else if(vect->type == FR_VECT_C)  input = vect->data[i];
   else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
   else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
   else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
   else if(vect->type == FR_VECT_1U) input = vect->dataU[i];

   stat->nDecay++;
   if(stat->nDecay == 1)
     {stat->meanA = input;
     stat->x2A = input*input;}
   else
     {decay = 1.-1./stat->nDecay;
     if(stat->decay < decay) decay = stat->decay;
     stat->meanA =  decay*stat->meanA + (1.-decay)*input;
     stat->x2A = decay*stat->x2A + (1.-decay)*input*input;}}

 stat->rmsA = sqrt(stat->x2A - stat->meanA*stat->meanA);

 return(stat);
}

/*-------------------------------------------------------------FrvStatProcV--*/
FrvStat* FrvStatProcV(FrvStat *stat, double input)
/*---------------------------------------------------------------------------*/
{double decay;

 if(stat  == NULL)
   {stat = FrvStatNew();
   if(stat == NULL) return(stat);}

 stat->nDecay++;
 if(stat->nDecay == 1)
   {stat->meanA = input;
   stat->x2A = input*input;}
 else
   {decay = 1.-1./stat->nDecay;
   if(stat->decay < decay) decay = stat->decay;
   stat->meanA =  decay*stat->meanA + (1.-decay)*input;
   stat->x2A = decay*stat->x2A + (1.-decay)*input*input;
   stat->rmsA = sqrt(stat->x2A - stat->meanA*stat->meanA);}

 return(stat);
}

/*-------------------------------------------------------------FrvStatReset--*/
void FrvStatReset(FrvStat *stat)
/*---------------------------------------------------------------------------*/
{
 if(stat == NULL) return;

 stat->nDecay = 0;
 stat->meanA = 0.;
 stat->rmsA = 0.;

 return;
}

/*----------------------------------------------------------FrvStatSetDecay--*/
void FrvStatSetDecay(FrvStat* stat, double decay)
/*---------------------------------------------------------------------------*/
/*   Change the decay value for the average computation                      */
/*   decay = 1. - 1./(number of values averaged)                             */
/*   decay = 0.  no average                                                  */
/*   decay = 1.  infinite average                                            */
/*   decay = .9  average over 10 values (with exponential weight)            */
/*---------------------------------------------------------------------------*/
{
 if(stat  == NULL)  return;

 if(decay < 0.) decay = 0.;
 if(decay > 1.) decay = 1.;
 stat->decay = decay;

 return;
}

/*------------------------------------------------------FrvStatSetDecayTime--*/
void FrvStatSetDecayTime(FrvStat* stat, double decayTime)
/*---------------------------------------------------------------------------*/
/*   Change the decay value for the average computation                      */
/*   decayTime is a time constant in seconds                                 */
/*   This way of changing the time constant is effective only if FrvStatProc
     is to be used, it cannot be used with FrvStatProcV                      */
/*---------------------------------------------------------------------------*/
{
 if(stat  == NULL)  return;

 if(decayTime < 0.) decayTime = 0.;
 stat->decayTime = decayTime;

 if(decayTime > 0.)
   {if(stat->dt > 0.)
     {stat->decay = 1.-stat->dt/decayTime;}
   else
     {stat->decay = -1.;}}
 else
   {stat->decay = 0.;}

 return;
}

/*-------------------------------------------------------------------FrvSub--*/
FrVect *FrvSub(FrVect *vect1,
               FrVect *vect2,
               FrVect *vect3,
                      char *newName)
/*---------------------------------------------------------------------------*/
/* This function computes vect3= vect1- vect2.                               */
/* The vector vect3 could be vect1 or vect2.                                 */
/* If vect3 = NULL, a new one is created. Its name is then newName.          */
/* It returns, the vector result or NULL in case of error.                   */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;

 if(vect1 == NULL) return(NULL);
 if(vect2 == NULL) return(NULL);
 if(vect1->type  != vect2->type)  return(NULL);
 if(vect1->nData != vect2->nData) return(NULL);

 if(vect3 != NULL)
      {if(vect3->type  != vect1->type)  return(NULL);
       if(vect3->nData != vect1->nData) return(NULL);
       vect = vect3;}
 else {vect = FrvClone(vect1, newName);}

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++)
        {vect->data[i] = vect1->data[i] - vect2->data[i];}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = vect1->dataS[i] - vect2->dataS[i];}}
 else if(vect->type == FR_VECT_8R)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] - vect2->dataD[i];}}
 else if(vect->type == FR_VECT_4R)
    {for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - vect2->dataF[i];}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = vect1->dataI[i] - vect2->dataI[i];}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = vect1->dataL[i] - vect2->dataL[i];}}
 else if(vect->type == FR_VECT_C8)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - vect2->dataF[i];}}
 else if(vect->type == FR_VECT_C16)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] - vect2->dataD[i];}}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUS[i] = vect1->dataUS[i] - vect2->dataUS[i];}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUI[i] = vect1->dataUI[i] - vect2->dataUI[i];}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataUL[i] = vect1->dataUL[i] - vect2->dataUL[i];}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++)
        {vect->dataU[i] = vect1->dataU[i] - vect2->dataU[i];}}

 return(vect);
}

/*--------------------------------------------------------------FrvZeroMean--*/
FrVect *FrvZeroMean(FrVect *vect1, FrVect *vectOut) 
/*---------------------------------------------------------------------------*/
/* This function computes vectOut= vect1+vect2. The vector vectOut could be  */
/* vect1 or vect2. If vectOut = NULL, a new on.    */
/* It returns, the output vector or NULL in case of error.                   */
/* If vectOut = NULL, the newName argument is ignored.                       */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 int i;
 double mean;
 if(vect1 == NULL) return(NULL);


 if(vectOut != NULL)
      {if(vectOut->type  != vect1->type)  return(NULL);
       if(vectOut->nData != vect1->nData) return(NULL);
       vect = vectOut;}
 else vect=vect1;
 if ((vect->type == FR_VECT_C) ||
     (vect->type == FR_VECT_8U)||
     (vect->type == FR_VECT_4U)||
     (vect->type == FR_VECT_2U)||
     (vect->type == FR_VECT_1U)) return (NULL);

 mean = FrVectMean(vect1);

 if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataS[i] = vect1->dataS[i] - mean;}}
 else if(vect->type == FR_VECT_8R)
    {for(i=0; i<vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i]- mean;}}
 else if(vect->type == FR_VECT_4R)
    {for(i=0; i<vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - mean;}}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataI[i] = vect1->dataI[i]- mean;}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++)
        {vect->dataL[i] = vect1->dataL[i] - mean;}}
 else if(vect->type == FR_VECT_C8)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataF[i] = vect1->dataF[i] - mean;}}
 else if(vect->type == FR_VECT_C16)
    {for(i=0; i<2*vect->nData; i++)
        {vect->dataD[i] = vect1->dataD[i] - mean;}}

 return(vect);
}
/*--------------------------------------------------------------FrvFillGaus--*/
int FrvFillGaus(FrVect *vect, double mean, double sigma) 
/*---------------------------------------------------------------------------*/
/* Fill a vector with random values according a gaussian distribution        */
/*---------------------------------------------------------------------------*/
{int i;
 
 if(vect == NULL) return(1);

 if(vect->type == FR_VECT_2S)
  {for(i=0; i<vect->nData; i++)  {vect->dataS[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_8R)
  {for(i=0; i<vect->nData; i++)  {vect->dataD[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_4R)
  {for(i=0; i<vect->nData; i++)  {vect->dataF[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_4S)
  {for(i=0; i<vect->nData; i++)  {vect->dataI[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_8S)
  {for(i=0; i<vect->nData; i++)  {vect->dataL[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_C8)
  {for(i=0; i<2*vect->nData; i++){vect->dataF[i] = mean + sigma*FrvGaus(0);}}
 else if(vect->type == FR_VECT_C16)
  {for(i=0; i<2*vect->nData; i++){vect->dataD[i] = mean + sigma*FrvGaus(0);}}
 else
   {return(2);}

 return(0);
}

/*----------------------------------------------------- UMrgausd -----*/
/* rangausd generates a pseudo random number (double) with normal     */
/* (Gaussian) distribution with zero mean and unit variance.          */
/* Author: B. Mours        27/02/91                                   */
/* Correction by: X. Grave 13/04/95                                   */
/*--------------------------------------------------------------------*/
double FrvGaus(int i)
{
 static long iset=0;
 static double gset;
 double fac,r,x,y;
 long a = 16807;
 long m = 2147483647;
 long q = 127773;
 long p = 2836;
 static long seed = 2147483646;
 long hi, lo;

 if (i != 0)  seed = i;
 
 if (iset == 1 )
    {iset = 0;
     return(gset);
    }
 
 iset = 1;
 do {
     hi   = seed / q;
     lo   = seed - hi * q;
     seed = a*lo - p*hi;
     if (seed <= 0) seed = seed + m;
     x    = -1. + 2. * (double)seed / (double)m;
     hi   = seed / q;
     lo   = seed - hi * q;
     seed = a*lo - p*hi;
     if (seed <= 0) seed = seed + m;
     y    = -1. + 2. * (double)seed / (double)m;
     r    = x*x + y*y;
    }
 while (r >= 1.0);
 
 if (r == 0.0)  r = 0.000001;
 fac  = sqrt(-2.*log(r)/r);
 gset = x * fac;

 return(y*fac);
}

/*---------------------------------------------------------------------------*/
double FrvUniform()
/*---------------------------------------------------------------------------*/
{/*
 *  // Copied from TRandom::Rndm
 * 
 * //  Machine independent random number generator.
 * //  Produces uniformly-distributed floating points between 0 and 1.
 * //  Identical sequence on all machines of >= 32 bits.
 * //  Periodicity = 10**8
 * //  Universal version (Fred james 1985).
 * //  generates a number in [0,1]
 */

   static long fSeed = 2147483646;
   const float kCONS = 4.6566128730774E-10;
   const int kMASK31 = 2147483647;
   int jy;
   float random;

   fSeed *= 69069;
      /* // keep only lower 31 bits */
   fSeed &= kMASK31;
      /* // Set lower 8 bits to zero to assure exact float */
   jy = (fSeed/256)*256;
   random = kCONS*jy;

   return (double)random;
}

