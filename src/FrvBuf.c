/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*--------------  FrvBuf.c by B.Mours Jul 12, 2013---------------------------*/

#include "FrvBuf.h"
#include <string.h>

#define FRV10E9 1000000000

/*---------------------------------define to use FFTW malloc/free functions--*/
#ifdef FFTW_MALLOC
void *fftw_free  (void *p);
void *fftw_malloc(size_t n);
void *FrvCalloc(size_t nobj, size_t size);

#define malloc fftw_malloc
#define calloc FrvCalloc
#define free   fftw_free
#endif
/*---------------------------------------------------------------------------*/

/*------------------------------------------------------------- FrvTimeAdd --*/
void  FrvTimeAdd(unsigned int *a, unsigned int *b, unsigned int *c)
/*---------------------------------------------------------------------------*/
{
  c[0] = a[0]+b[0];  /*--- seconds field */
  c[1] = a[1]+b[1];  /*--- nanosecond field */

  if(c[1] >= FRV10E9)
    {c[1] -= FRV10E9;
     c[0] ++;}
   
 return;
}
/*------------------------------------------------------------- FrvBufFree --*/
void  FrvBufFree(struct FrvBuf* buffer)
/*---------------------------------------------------------------------------*/
{
  if(buffer == NULL) return;

  if(buffer->input != NULL) 
    {buffer->input->data = buffer->work;
     FrVectFree(buffer->input);}

  if(buffer->output != NULL)
    {buffer->output->data = malloc(1);
     FrVectFree(buffer->output);}

  free(buffer);

  return;
}

/*------------------------------------------------------------ FrvBufFeed ---*/
int FrvBufFeed(FrvBuf *buffer, 
               FrVect *vect)
/*---------------------------------------------------------------------------*/
/*  Copy the data in the output structure 
  Returns 0 if at least one output is ready
          1 if more call to FrvBufFeed are needed
          2 in case of error                
          3 in case of reset due to timing missmatch
          4 when waiting to start wit a gps aligned vector                   */
/*---------------------------------------------------------------------------*/
{unsigned int vectSize, workSize, wSize;
 double dt, vTime;
 int flag;
 
 if(buffer == NULL) return(2);
 if(vect == NULL)   return(2);
 if(vect->compress != 0) FrVectExpand(vect);

 //----if requested do not start feeding until the input vector is GPS aligned
 //    this is working even after a buffer reset
 if(buffer->gpsOffset >= 0 &&
    buffer->last  == 0 &&
    buffer->first == 0) {
    int duration = buffer->nDataOut * vect->dx[0];
    if(((int)vect->GTime) % duration != buffer->gpsOffset) return(4);}

          /*----------------------------------- init the object if needed ---*/

 if(buffer->work == NULL)
   {if(FrvBufInit(buffer, vect) != 0) return(2);
    buffer->output->dx[0] = buffer->decimate*buffer->output->dx[0];}

         
        /*------------ check timing continuity  ------------------------------*/
 flag = 0;
 
 vTime = vect->GTime + vect->startX[0];

 if((vect->GTime > 0) && (buffer->lastGTime > 0))
    {dt = buffer->lastGTime - vTime;
     if(dt < 0.) dt = -dt;
     if(dt > vect->dx[0]) 
       {flag=1;
	    /*-- reset buffer: forget existing (obsolete) data --*/
        buffer->first = 0;
        buffer->last  = 0;
        if(buffer->gpsOffset >= 0) { 
          int duration = buffer->nDataOut * vect->dx[0];
          if(((int)vect->GTime) % duration != buffer->gpsOffset) return(4);}}}
     
     
             /*---------------shift existing data to remove the starting gap ---*/

 wSize = buffer->output->wSize;

 if(buffer->first != 0)
   {memmove(buffer->work, buffer->work+buffer->first*wSize,
                       (buffer->last - buffer->first)*wSize);

    buffer->last -= buffer->first;
    buffer->first = 0;}

           /*--------------------------- Increase working space if needed ---*/

 vectSize = vect->nData / buffer->decimate;
 workSize = buffer->last + vectSize;

 if(workSize > buffer->workSize) {
     buffer->work = realloc(buffer->work, workSize*wSize);
     if(buffer->work  == NULL) return(1);
     buffer->workSize = workSize;
     buffer->first = 0;}
  
            /*---- convert the input vector if needed and copy the data------*/
            /*--------------------- at the end of the buffer ----------------*/

 buffer->input->data = buffer->work + buffer->last * wSize;

 if(buffer->decimate != 1)
    {FrVectMap(buffer->input);
     buffer->input->nData = vect->nData;
     if(FrVectDecimate(vect, buffer->decSign*buffer->decimate, 
                             buffer->input)==NULL) return(2);}
 else if(vect->type != buffer->input->type)
    {FrVectMap(buffer->input);
     buffer->input->nData = vect->nData;
     if(FrvCopyTo(vect, 1., buffer->input) == NULL) return(2);}
 else
    {memcpy(buffer->input->data, vect->data, vectSize*wSize);}
 
 buffer->last += vectSize;

 buffer->lastGTime = vTime + vect->nData*vect->dx[0];

         /*------------------------------------- is the data ready to use?---*/

 if(flag == 1) return(3);
 if(FrvBufStillToGo(buffer) > 0) return(1);
 return(0);
}

/*------------------------------------------------------------ FrvBufFeedN---*/
int FrvBufFeedN(FrvBuf *buffer, 
                FrVect *vect,
                int iStart,
                int nDataNew)
/*---------------------------------------------------------------------------*/
/*  Feed only part of the vector vect to the buffer (nDataNew elements 
    starting from iStart). The returns code are the same as FrvBuffFeed.     */
/*---------------------------------------------------------------------------*/
{char *data;
 int nData, errcode;
 double startX;

 if(vect   == NULL)        return(2);
 if(iStart < 0)            return(2);
 if(iStart >= vect->nData) return(2);
 
                              /*----- save the original vector paramters ---*/
 data   = vect->data;
 nData  = vect->nData;
 startX = vect->startX[0];
                             /*------------------- fack a shorter vector ---*/
 vect->nData     = nDataNew;
 vect->nx[0]     = nDataNew;
 vect->data      += iStart * vect->wSize;
 vect->startX[0] += iStart * vect->dx[0];
 FrVectMap(vect);
                             /*------------------------- feed the buffer ---*/
 errcode = FrvBufFeed(buffer, vect);
                             /*------------- restor the vector paramters ---*/
 vect->data      = data;
 vect->nData     = nData;
 vect->nx[0]     = nData;
 vect->startX[0] = startX;
 FrVectMap(vect);
 
 return(errcode);}

/*------------------------------------------------------------ FrvBufGetNext-*/
int FrvBufGetNext(FrvBuf *buffer)
/*---------------------------------------------------------------------------*/
/*  Prepare the next output buffer
    Returns: 1 if a call to FrvBufFeed is needed or in case of error
             0 if a vector is available                                      */
/*---------------------------------------------------------------------------*/
{double offset;
 int index;

 if(buffer == NULL) return(1);
 if(buffer->work == NULL) return(1);

             /*----------------------- not enough data in the ouput buffer---*/

 if(FrvBufStillToGo(buffer))  return(1);

             /*-------------------------------- prepare the output buffer----*/

 if(buffer->intDelay == FR_NO)
      index = buffer->first+buffer->delay;
 else index = buffer->first;
 buffer->output->data = buffer->work + index*buffer->output->wSize;
 FrVectMap(buffer->output);

             /*------------------------------ set the output vector time ----*/

 offset = (buffer->last - buffer->first)*buffer->output->dx[0];
 buffer->output->GTime = buffer->lastGTime - offset;

            /*-------------------- shift the start index for the next use ---*/

 buffer->first += buffer->step;

 return(0);
}
/*------------------------------------------------------------- FrvBufInit --*/
int FrvBufInit(FrvBuf *buffer,
               FrVect *vect)
/*---------------------------------------------------------------------------*/
/* Returns 0 in case success; 1 in case of problem.                          */
/*---------------------------------------------------------------------------*/
{
 if(vect == NULL) return(1);

 if(buffer->nDataOut < 1) buffer->nDataOut = vect->nData;
 if(buffer->outType  < 0) buffer->outType  = vect->type;

       /*---- create the input vector(the space used is the working space)---*/

 buffer->input  = FrVectNew1D(vect->name,  buffer->outType, 
                              vect->nData /buffer->decimate,
                              vect->dx[0], vect->unitX[0], vect->unitY);
 if(buffer->input  == NULL) return(1);

 free(buffer->input->data);
 buffer->input->data = NULL;

       /*---- create the output vector(the space used is the working space)---*/

 buffer->output = FrVectNew1D(vect->name,  buffer->outType, buffer->nDataOut,
                              vect->dx[0], vect->unitX[0], vect->unitY);
 if(buffer->output == NULL) return(1);

 free(buffer->output->data);
 buffer->output->data = NULL;
 buffer->output->startX[0] = 0.;

      /*---------------------------------------allocate the working space ---*/

 if(buffer->step > buffer->nDataOut)
      {buffer->workSize = buffer->step     + buffer->delay + vect->nData;}
 else {buffer->workSize = buffer->nDataOut + buffer->delay + vect->nData;}

 buffer->work = malloc(buffer->workSize * buffer->output->wSize);
 if(buffer->work  == NULL) return(1);

         /*--------------------------------------------- init timing info ---*/

 buffer->lastGTime = vect->GTime + vect->startX[0];

 return(0);
}

/*-------------------------------------------------------------- FrvBufNew --*/
FrvBuf* FrvBufNew(int nDataOut,
                  int step,
                  int outType,
                  int decimate,
                  int delay)
/*---------------------------------------------------------------------------*/
/* Create the buffer structure                                               */
/*---------------------------------------------------------------------------*/
{FrvBuf *buffer;

 buffer = (struct FrvBuf *) (calloc(1,sizeof(struct FrvBuf)));
 if(buffer == NULL) return(NULL);
 
 buffer->nDataOut = nDataOut;
 buffer->step     = step;
 buffer->intDelay = FR_NO;
 buffer->gpsOffset = -1;

 if((outType != -1) && 
    (outType != FR_VECT_2S) && 
    (outType != FR_VECT_4S) && 
    (outType != FR_VECT_4R) && 
    (outType != FR_VECT_8R)) return(NULL);
 buffer->outType  = outType;

 buffer->decSign  = 1.;
 if(decimate == 0)   
        {buffer->decimate = 1;}
 else if(decimate < 0)
        {buffer->decimate = -decimate;
         buffer->decSign  = -1.;}
 else   {buffer->decimate =  decimate;}

 if(delay < 0) delay = 0;
 buffer->delay  = delay;

 buffer->first = 0;
 buffer->last  = 0;

 return(buffer);
}

/*-------------------------------------------------------------- FrvBufNewID-*/
FrvBuf* FrvBufNewID(int nDataOut,
                    int step,
                    int outType,
                    int decimate,
                    int delay)
/*---------------------------------------------------------------------------*/
/* Create the buffer structure                                               */
/*---------------------------------------------------------------------------*/
{FrvBuf *buffer;

 buffer = FrvBufNew(nDataOut, step, outType, decimate, delay);
 if(buffer == NULL) return(NULL);

 buffer->intDelay = FR_YES;

 return(buffer);}

//----------------------------------------------------------------------------
void FrvBufSetAligned(FrvBuf *buffer, int offset)
//----------------------------------------------------------------------------
{
  if(buffer != NULL) buffer->gpsOffset = offset;

  return;
}
/*------------------------------------------------------- FrvBufStillToGo ---*/
int FrvBufStillToGo(FrvBuf *b)
/*---------------------------------------------------------------------------*/
/*  This function returns the number of data sample to be given to the buffer
    in order to fill up the buffer and to be able to output a vector         */
/*---------------------------------------------------------------------------*/
{int stg;

 if(b == NULL) return(-1);

 stg = (b->nDataOut - (b->last - b->first - b->delay))* b->decimate;
 if(stg < 0) stg = 0;
 if(b->nDataOut == 0) stg = -1;

 return(stg);
}
