/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef FRVCORR
#define FRVCORR

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
/*---------------------------------------------------------------------------*/
/*  FrvCorr.h  May 31, 2001 by I. Ferrante (from Siesta file USignal.*)      */
/*---------------------------------------------------------------------------*/

#include "FrameL.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FrvCorr FrvCorr;

struct FrvCorr {
  /* performs the correlation according to c_k= sum_i v1_i * v2_i+k*/
                             /*--------------------- input data -------------*/
  int normalize;             /* 0 -> unbiased  1-> biased */
  int maxlag;                /* maximum lag                                  */
  double * past1;            /* past data vector 1                           */
  double * past2;            /* past data vector 2                           */
  struct FrVect * average;   /* average correlation                          */
  struct FrVect  * present;  /* correlation for the present frame            */
  double * present0;         /* pointer to zero lag present frame correlation*/
  double * average0;         /* pointer to zero lag average  correlation     */
  int nCall;
  char error[256];

};


void        FrvCorrFree(FrvCorr* corr);
int         FrvCorrProc(FrvCorr *corr, FrVect *vect1, FrVect * vect2);
int         FrvCorrInit(FrvCorr *corr, FrVect *vect1, FrVect * vect2);
FrvCorr*    FrvCorrNew(int maxlag, int normalize);
void        FrvCorrSetPast(FrvCorr* corr, double * past1, double * past2, int flag);
#ifdef __cplusplus
}
#endif

#endif
