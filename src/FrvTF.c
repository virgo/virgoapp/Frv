/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*  FrvTF.c by  F. Marion and B. Mours    March 01, 2005           */

#include "FrvTF.h"
#include "FrvMath.h"
#include <string.h>
#include <math.h>

/*------------------------------------------------------------ FrvCoGapFree--*/
void FrvCoGapFree(FrvCoGap *coGap)
/*---------------------------------------------------------------------------*/
{
 if(coGap == NULL) return;

 if(coGap->channel1 != NULL) free(coGap->channel1);
 if(coGap->channel2 != NULL) free(coGap->channel2);
 if(coGap->tf != NULL) FrvTFFree(coGap->tf);
 free(coGap);

 return;}

/*------------------------------------------------------------- FrvCoGapNew--*/
FrvCoGap* FrvCoGapNew(char *channel1,
                      char *channel2,
                      double FFTduration,
                      int    nAverage,
                      double freqMin,
                      double freqMax,
                      double threshold)
/*---------------------------------------------------------------------------*/
{FrvCoGap *coGap;

 coGap = (FrvCoGap *) (calloc(1, sizeof(FrvCoGap)));
 if(coGap == NULL) return(NULL);

 FrStrCpy(&(coGap->channel1),channel1);
 FrStrCpy(&(coGap->channel2),channel2);
 coGap->nAverage    = nAverage;
 coGap->freqMin     = freqMin;
 coGap->freqMax     = freqMax;
 coGap->threshold   = threshold;

 coGap->tf = FrvTFNewT("C", FFTduration, nAverage);
 if(coGap->tf == NULL) 
   {FrvCoGapFree(coGap);
    return(NULL);}
           
 return(coGap);
}
/*------------------------------------------------------------ FrvCoGapProc--*/
int FrvCoGapProc(FrvCoGap* coGap, FrameH *frame)
/*---------------------------------------------------------------------------*/
{ FrVect *vect1, *vect2, *coherence;
  int i, iMin, iMax, iGap, gap, gapMax, nMissing;
  double sum;

  if(coGap == NULL) return(-1);
  if(frame == NULL) return(-1);

          /*-------- reset the coherence in case of vector discontinuity ----*/

  vect1 = FrameFindVect(frame, coGap->channel1);
  vect2 = FrameFindVect(frame, coGap->channel2);

  if(vect1 == NULL || vect2 == NULL) return(-2);

       /*--------------------------------------------- process the data -----*/

  FrvTFProc(coGap->tf, vect1, vect2);
  nMissing = coGap->nAverage - coGap->tf->nCall;
  if(nMissing > 0) return(nMissing);

       /*-------------- search for the maximim gap in the frequency band ----*/

  coherence = coGap->tf->coherence;
  iMin = FrVectGetIndex(coherence, coGap->freqMin);
  iMax = FrVectGetIndex(coherence, coGap->freqMax);
  iGap   = 0;
  gap    = 0;
  gapMax = 0;
  sum    = 0;
  for(i=iMin; i<iMax; i++)
    {sum += coherence->dataD[i];
     if(coherence->dataD[i] > coGap->threshold)
      {if(gap != 0)
	{if(gap > gapMax) gapMax = gap;
	gap = 0;}}
      else
	{if(gap == 0) iGap = i;
         gap++;}}

  if(iMax != iMin) 
       coGap->meanCoherence = sum/(iMax-iMin);
  else coGap->meanCoherence = 0.;
  coGap->maxFreqGap = gapMax*coherence->dx[0];
  coGap->startGap   = iGap  *coherence->dx[0];
  coGap->GTime = frame->GTimeS+1.e-9*frame->GTimeN;
 
  FrvTFReset(coGap->tf);  /*--- reset the coherence to get fresh data -------*/

  return(0);}

/*-------------------------------------------------------------- FrvTFError--*/
void FrvTFError(FrvTF *tf)
/*---------------------------------------------------------------------------*/
/*   Returns: 0 if OK or an error code in case of problem.                   */
/*---------------------------------------------------------------------------*/
{double dnu, *errorM, *errorP, *coherence, *modulus, coefA, coefB, term;
 int nData, i;

 if(tf == NULL) return;
 if(tf->optionC != FR_YES) return; 

                /*----- create output structures if they are not available---*/

 nData = tf->modulus->nData;

 if(tf->errorM == NULL)
   { dnu   = tf->modulus->dx[0];
     tf->errorM = FrVectNew1D("modulusErr",FR_VECT_8R, nData, dnu, 
                                       "Frequency [Hz]", "modulus Error");
     tf->errorP  = FrVectNew1D("phaseErr",FR_VECT_8R, nData, dnu,
                                       "Frequency [Hz]", "phase error");
    if(tf->errorM == NULL) return;
    if(tf->errorP == NULL) return;}

               /*------------------------------------ compute the errors ---*/

 errorM    = tf->errorM->dataD;
 errorP    = tf->errorP->dataD;    
 coherence = tf->coherence->dataD;
 modulus   = tf->modulus->dataD;

 coefA = .85/sqrt(tf->nCall);
 coefB = .88/sqrt(tf->nCall);

 for(i=0; i<nData; i++)  
   {term = sqrt((1.-coherence[i])/coherence[i]);
    errorM[i] = coefA*term*modulus[i];
    errorP[i] = coefB*term;}

 return;}

/*-------------------------------------------------------------- FrvTFFree--*/
void FrvTFFree(FrvTF *tf)
/*---------------------------------------------------------------------------*/
/*  Free the TF object.                                                      */
/*---------------------------------------------------------------------------*/
{
 if(tf == NULL) return;

 FrvBufFree(tf->bufferS);
 FrvBufFree(tf->bufferR);

 FrvRFFTFree(tf->fftS);
 FrvRFFTFree(tf->fftR);

 FrVectFree(tf->output);
 FrVectFree(tf->correlation);
 FrVectFree(tf->modulus);
 FrVectFree(tf->phase);

 if(tf->coherence != NULL) FrVectFree(tf->coherence);
    
 free(tf);
                        
 return;
}
/*---------------------------------------------------------------- FrvTFIni--*/
int FrvTFInit(FrvTF *tf,
              FrVect *signal, 
              FrVect *ref)
/*---------------------------------------------------------------------------*/
/*   Returns: 0 if OK or an error code in case of problem.                   */
/*---------------------------------------------------------------------------*/
{char name[256];
 double dnu;
 int nData, outS, outR, i, outSizeSignal, outSizeRef;


 if(tf     == NULL) return(1);
 if(signal == NULL) return(1);
 if(ref    == NULL) return(1);       

            /*------- create FFT buffer object -----*/

 if(tf->duration != 0) 
    {outS = (tf->duration+.5*signal->dx[0])/signal->dx[0];
     outR = (tf->duration+.5*ref   ->dx[0])/ref->dx[0];
     if(outR < outS) tf->outSize = outR;
     else            tf->outSize = outS;}
 else
   {outSizeSignal = signal->nData;
    outSizeRef    = ref   ->nData;
    if(tf->outSize < 1){
      if(outSizeSignal <= outSizeRef) tf->outSize = outSizeSignal; 
      else                            tf->outSize = outSizeRef;}
    outS = tf->outSize;
    outR = tf->outSize;
    if(outSizeSignal <= outSizeRef) tf->duration = tf->outSize*signal->dx[0];
    else                            tf->duration = tf->outSize*ref->dx[0];}

 tf->bufferS = FrvBufNew(outS, outS/2, FR_VECT_8R, tf->decimate, 0);
 tf->bufferR = FrvBufNew(outR, outR/2, FR_VECT_8R, tf->decimate, 0);

 if(tf->bufferS == NULL) return(2);
 if(tf->bufferR == NULL) return(2);

            /*------- create FFT object -----*/

 if((tf->fftS = FrvRFFTNewT("HAN",tf->duration,tf->nAverage)) == NULL) return(2);
 if((tf->fftR = FrvRFFTNewT("HAN",tf->duration,tf->nAverage)) == NULL) return(2);

                 /*--------- create output structures ----------------------*/

 sprintf(name,"TF(%s/%s)",signal->name,ref->name);
 nData = tf->outSize/2;
 if(signal->dx[0] >= ref->dx[0])
   dnu = 1./(2*nData*tf->decimate*signal->dx[0]);
 else
   dnu = 1./(2*nData*tf->decimate*ref->   dx[0]);

 tf->output  = FrVectNew1D(name, FR_VECT_C16,nData,dnu,"Frequency [Hz]","T.F");

 tf->correlation = FrVectNew1D("correlation", FR_VECT_C16,nData,dnu,
                                         "Frequency [Hz]","T.F correlation");

 tf->modulus = FrVectNew1D("modulus",FR_VECT_8R, nData, dnu, "Frequency [Hz]",
                           "modulus");
 tf->phase   = FrVectNew1D("phase",FR_VECT_8R, nData, dnu, "Frequency [Hz]",
                           "phase [rad]");
 if(tf->output      == NULL) return(1);
 if(tf->correlation == NULL) return(1);
 if(tf->modulus     == NULL) return(1);
 if(tf->phase       == NULL) return(1);
    
 if(tf->optionC == FR_YES) 
   {tf->coherence = FrVectNew1D("coherence",FR_VECT_8R, nData, dnu,
                                "Frequency[Hz]", "coherence");
    if(tf->coherence == NULL) return(1);}

 for(i=0; i<2*tf->correlation->nData; i++)  {tf->correlation->dataD[i] = 0.;}
 for(i=0; i<tf->phase->nData; i++)  {tf->phase->dataD[i] = 0.;}
    
         /*-------------------- adjust bin origin ---------------------------*/

 tf->output     ->startX[0] = 0.5*dnu;
 tf->correlation->startX[0] = 0.5*dnu;
 tf->modulus    ->startX[0] = 0.5*dnu;
 tf->phase      ->startX[0] = 0.5*dnu;

 return(0);
}
/*-------------------------------------------------------------- FrvTFNew -*/
FrvTF* FrvTFNew(char* option, int outSize, int decimate)
/*---------------------------------------------------------------------------*/
{FrvTF *tf;

 tf = (FrvTF *) (calloc(1, sizeof(FrvTF)));
 if(tf == NULL) return(NULL);

 tf->optionC = FR_NO;
 tf->optionE = FR_NO;
 if(option != NULL)
   {if(strchr(option,'C') != NULL) tf->optionC = FR_YES;
    if(strchr(option,'E') != NULL) tf->optionE = FR_YES;}

 tf->outSize   = outSize;
  
 if(decimate < 1)   decimate = 1;
 tf->decimate  = decimate;
 tf->nAverage  = 1000;
 tf->nCall   = 0;
                        
 return(tf);
}
/*-------------------------------------------------------------- FrvTFNewT- -*/
FrvTF* FrvTFNewT(char* option, double duration, int nAverage)
/*---------------------------------------------------------------------------*/
{FrvTF *tf;

 tf = FrvTFNew(option, 0, 0);
 if(tf == NULL) return(NULL);
 tf->duration = duration;
 tf->nAverage = nAverage;
                        
 return(tf);
}
/*-------------------------------------------------------------- FrvTFProc--*/
int FrvTFProc(FrvTF *tf, 
              FrVect *signal, 
              FrVect *ref)
/*---------------------------------------------------------------------------*/
/*   Returns: 0 if OK or an error code in case of problem                    */
/*---------------------------------------------------------------------------*/
{
 int i, nDataS, nDataR, nDataMin;
 double  decay, modNum,
         *output, *correlation, *modulus, *coherence, *signalA, *noiseA;

 if(tf     == NULL)  return(1);
 if(signal == NULL)  return(1);
 if(ref    == NULL)  return(1);

          /*------------ initialized tf object if needed ------------------*/

 if(tf->output == NULL) {if(FrvTFInit(tf,signal, ref) != 0) return(3);}

          /*---------------------- feed data --------------------------------*/

 FrvBufFeed(tf->bufferS,signal);     
 FrvBufFeed(tf->bufferR,ref);     
 
          /*---------------------------- now loop over the sub samples ------*/

 while(FrvBufGetNext(tf->bufferS) == 0)

   {if(FrvBufGetNext(tf->bufferR) != 0) return(4);

    tf->nCall++;
    if(tf->nCall<tf->nAverage) decay = 1. - 1./tf->nCall;
    else                       decay = 1. - 1./tf->nAverage;

              /*------------------------------- compute FFT ----------------*/

    FrvRFFTFor(tf->fftS, tf->bufferS->output);
    FrvRFFTFor(tf->fftR, tf->bufferR->output);

              /*--------- adjust vector length in case they are different ---*/

    nDataS = tf->fftS->output->nData;
    nDataR = tf->fftR->output->nData;
    nDataMin = nDataS;
    if(nDataMin > nDataR) nDataMin = nDataR;
    tf->fftS->output->nData = nDataMin;
    tf->fftR->output->nData = nDataMin;

              /*------------------ compute the current TF correlation--------*/
              /*------------- W: we make temporary use of tf->output---------*/

    FrvMultConj(tf->fftS->output,tf->fftR->output,tf->output,NULL);

              /*--------------------- compute the mean correlation-----------*/

    output    = tf->output->dataD;
    correlation = tf->correlation->dataD;

    for(i=0; i<2*tf->output->nData; i++) 
        {correlation[i] = decay*correlation[i] +(1.-decay)*output[i];}

              /*-------- -------- compute the mean modulus and coherance------*/

    modulus   = tf->modulus->dataD;
    signalA   = tf->fftS->amplitudeA->dataD;
    noiseA    = tf->fftR->amplitudeA->dataD;
    if(tf->optionC == FR_YES) 
         {coherence = tf->coherence->dataD;}
    else {coherence = NULL;}

    for(i=0; i<tf->output->nData; i++) 
        {modNum = sqrt(correlation[2*i]  *correlation[2*i]  +
                       correlation[2*i+1]*correlation[2*i+1]);
         modulus[i] = modNum/(noiseA[i]*noiseA[i]);
         if(tf->optionC == FR_YES) 
                coherence[i] = modulus[i]*modNum/(signalA[i]*signalA[i]);}

              /*------------------------ compute the mean phase-------------*/

    FrvPhase(tf->correlation,tf->phase,NULL);
 
              /*--------------------------- compute the current TF ----------*/

    FrvDivide(tf->fftS->output,tf->fftR->output, tf->output, NULL);

    tf->fftS->output->nData = nDataS;
    tf->fftR->output->nData = nDataR;}


               /*------------------- compute the TF error if requested-------*/

 if(tf->optionE == FR_YES) FrvTFError(tf); 
 
                     /*------- Set start time for all the series defined --- */

 tf->output ->GTime = signal->GTime;
 tf->modulus->GTime = signal->GTime;
 if(tf->coherence != NULL) tf->coherence->GTime = signal->GTime;
 if(tf->errorM    != NULL) tf->errorM   ->GTime = signal->GTime;
 if(tf->errorP    != NULL) tf->errorP   ->GTime = signal->GTime;

 return(0);
}

/*------------------------------------------------------------- FrvTFReset --*/
void FrvTFReset(FrvTF* tf)
/*---------------------------------------------------------------------------*/
/*   Reset the averaging process                                             */
/*---------------------------------------------------------------------------*/
{
 if(tf  == NULL)  return;

 tf->nCall = 0;  
 if(tf->fftS != NULL) tf->fftS->nFFT = 0;
 if(tf->fftR != NULL) tf->fftR->nFFT = 0;

 return;}

