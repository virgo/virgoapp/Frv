/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#define _POSIX_SOURCE 
/*---------------------------------------------------------------------------*/
/*  FrvCopy.c by B.Mours LAPP (Annecy)         Jan 12, 2004                  */
/*---------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h> 
#include <stdarg.h>
#include <ctype.h>
#include "FrvCopy.h"
#if (defined(_WIN32) || defined(__WIN32__))
#include <winsock.h>
typedef unsigned __int32 uint32_t;
typedef unsigned __int16 uint16_t;
#else
#include <netinet/in.h>
#endif

/*---------------------------------define to use FFTW malloc/free functions--*/
#ifdef FFTW_MALLOC
void *fftw_free  (void *p);
void *fftw_malloc(size_t n);
void *FrvCalloc(size_t nobj, size_t size)
{void *mem;
 mem = fftw_malloc(nobj*size);
 if(mem == NULL) return(NULL);
 memset(mem,0,nobj*size);
 return(mem);};
#define malloc fftw_malloc
#define calloc FrvCalloc
#define free   fftw_free
#endif
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------FrvClone--*/
FrVect *FrvClone(FrVect *vectin,
                 char *newName)
/*---------------------------------------------------------------------------*/
/* This function create a new vector (FrVect structure and data area);       */
/* It copy the header information but not the data.                          */
/* If newName = NULL, the original vector name is used.                      */
/* This function returns null in case of problems like malloc failed.        */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 long i;

  if(vectin == NULL) return(NULL);

   vect = (FrVect *) calloc(1,sizeof(FrVect));
   if(vect == NULL)  
        {return(NULL);}
   else {vect->classe = FrVectDef();}

   if(newName != NULL)
        {if(FrStrCpy(&vect->name,      newName) == NULL) return(NULL);}
   else {if(FrStrCpy(&vect->name, vectin->name) == NULL) return(NULL);}

   vect->compress = 0;
   vect->type     = vectin->type;
   vect->nData    = vectin->nData;
   vect->nBytes   = vectin->nBytes;
   vect->nDim     = vectin->nDim;
   vect->nx     = malloc(vectin->nDim*sizeof(long));
   vect->unitX  = malloc(vectin->nDim*sizeof(char *));
   vect->startX = malloc(vectin->nDim*sizeof(double));
   vect->dx     = malloc(vectin->nDim*sizeof(double));
   if(vect->nx     == NULL || 
      vect->unitX  == NULL || 
      vect->startX == NULL || 
      vect->dx     == NULL) return(NULL);

   for(i=0; i<vectin->nDim; i++)
     {vect->nx[i]     = vectin->nx[i];
      vect->startX[i] = vectin->startX[i];
      vect->dx[i]     = vectin->dx[i];
      FrStrCpy(&vect->unitX[i], vectin->unitX[i]);
      if(vect->unitX[i] == NULL && vectin->unitX[i] != NULL) return(NULL);}


   FrStrCpy(&vect->unitY, vectin->unitY);

   vect->wSize = vectin->wSize;
   vect->space = vectin->space;
   vect->data = malloc(vectin->space*vectin->wSize);
   if(vect->data == NULL) 
      {FrError(3,"FrVectCopy","malloc failed");
       return(NULL);}

   FrVectMap(vect);

   vect->GTime = vectin->GTime;
   vect->next = NULL;

   return(vect);
}

/*-------------------------------------------------------------FrvDecimateD--*/
FrVect *FrvDecimateD(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* double. The size of the output vector is nGroup time smaller than the size*/
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_8R, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateF--*/
FrVect *FrvDecimateF(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* float. The size of the output vector is nGroup time smaller than the size */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_4R, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateI--*/
FrVect *FrvDecimateI(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* int. The size of the output vector is nGroup time smaller than the size   */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_4S, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateS--*/
FrVect *FrvDecimateS(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* short. The size of the output vector is nGroup time smaller than the size */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_2S, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
//--------------------------------------------------------------------------
int FrvWriteWav(FrVect* vectIn, char *fileName) 
//----------write a wav file for the input vector, fileNName is optional----
{
  if(vectIn == NULL) return(-1);

  char* fName = fileName;
  if(fName == NULL) {
    fName = (char*) malloc(strlen(vectIn->name) + 30);
    if(fName == NULL) return(-2);
    int duration = vectIn->nData * vectIn->dx[0];
    sprintf(fName,"%s_%.0f_%d.wav", vectIn->name, vectIn->GTime, duration);}

  FILE *fp = fopen(fName,"w");
  if(fp == NULL) return(-3);
  if(fName != fileName) free(fName);

  // make a local copy to get a normalized vector
  FrVect* vect = FrVectCopyToF(vectIn, 1., NULL);
  if(vect == NULL) return(-4);

  double mean = FrVectMean(vect);
  double min, max;
  FrVectMinMax(vect, &min, &max);
  min -= mean;
  max -= mean;
  if(-min > max) max = -min;
  int i;
  for(i=0; i< vect->nData; i++) {
    vect->dataF[i] -= mean;
    vect->dataF[i] /= max;};

  int sampleRate = 1./vect->dx[0];
  int numChannels = 1;
  int nBytes =sizeof(float)*vect->nData;

typedef struct WavHeader {
  uint32_t ChunkID;
  uint32_t ChunkSize;
  uint32_t Format;
  uint32_t Subchunk1ID;
  uint32_t Subchunk1Size;
  uint16_t AudioFormat;
  uint16_t NumChannels;
  uint32_t SampleRate;
  uint32_t ByteRate;
  uint16_t BlockAlign;
  uint16_t BitsPerSample;
  uint32_t Subchunk2ID;
  uint32_t Subchunk2Size;
} WavHeader;

  // prepare WAV header
  WavHeader h;
  h.ChunkID = htonl(0x52494646); // "RIFF"
  h.ChunkSize = 36 + nBytes; 
  h.Format = htonl(0x57415645); // "WAVE"
  h.Subchunk1ID = htonl(0x666d7420); // "fmt "
  h.Subchunk1Size = 16; // PCM
  h.AudioFormat = 3; // 1 PCM, 3 IEEE float
  h.NumChannels = 1;
  h.SampleRate = sampleRate;
  h.ByteRate   = numChannels + sampleRate * 4; // assumes 4 bytes float
  h.BlockAlign = numChannels*4; 
  h.BitsPerSample = 32; 
  h.Subchunk2ID = htonl(0x64617461); // "data"
  h.Subchunk2Size = nBytes; 

  // write WAV header
  fwrite(&h, sizeof(WavHeader), 1, fp);

  // write data block
  fwrite(vect->data, sizeof(float), vect->nData, fp);

  // close file
  fclose(fp);

  FrVectFree(vect);

  return(0);
}
