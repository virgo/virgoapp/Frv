/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*--------------------------------------------- FrvFDFilter.c  Jun 06, 2012 */
#include <math.h>
#include "FrvFDFilter.h"
#include "Frv.h"
#include "fftw3.h"

double SKIP = .5;  /**< fraction of analysis window ignored on each side*/

/*---------------------------------------------------------------------------*/
int FrvFDFilterAddHole(FrvFDFilter *fdf, 
                       double fMin, double fMax, 
                       double scale, double delay)
/*---------------------------------------------------------------------------*/
{FrvFDFHole *hole;

 if(fdf == NULL) return(-1);

 hole = (FrvFDFHole *) calloc(1,sizeof(FrvFDFHole));
 if(hole == NULL) return(-2);

 hole->fMin = fMin;
 hole->fMax = fMax;
 hole->scale = scale;
 hole->delay = delay;

 hole->next = fdf->hole;
 fdf->hole  = hole;

 return(0);
}
/*---------------------------------------------------------------------------*/
int FrvFDFilterAddPhaseBump(FrvFDFilter *fdf, 
                       double freq, double width, double dPhi)
/*---------------------------------------------------------------------------*/
{FrvFDFPhaseBump  *phaseBump;

 if(fdf == NULL) return(-1);

 phaseBump = (FrvFDFPhaseBump *) calloc(1,sizeof(FrvFDFPhaseBump));
 if(phaseBump == NULL) return(-2);

 phaseBump->freq  = freq;
 phaseBump->width = width;
 phaseBump->dPhi  = dPhi;

 phaseBump->next = fdf->phaseBump;
 fdf->phaseBump  = phaseBump;

 return(0);
}

/*---------------------------------------------------------------------------*/
int FrvFDFilterAddFilter2(FrvFDFilter *fdf,
			  double fStart, double scale, double delay,
                          double a2, double a1, double a0,
                          double b2, double b1, double b0)
/*---------------------------------------------------------------------------*/
{FrvFDFilter2 *filter2;

 if(fdf == NULL) return(-1);

 filter2 = (FrvFDFilter2 *) calloc(1,sizeof(FrvFDFilter2));
 if(filter2 == NULL) return(-2);
 filter2->next = fdf->filter2;
 fdf->filter2  = filter2;

 filter2->fStart = fStart;
 filter2->scale = scale;
 filter2->delay = delay;
 filter2->a2 = a2;
 filter2->a1 = a1;
 filter2->a0 = a0;
 filter2->b2 = b2;
 filter2->b1 = b1;
 filter2->b0 = b0;

 return(0);
}

/*---------------------------------------------------------------------------*/
int FrvFDFilterAddNotch(FrvFDFilter *fdf, double freq)
/*---------------------------------------------------------------------------*/
{
 if(fdf == NULL) return(-1);

 /*--- to be completed ---*/
 return(0);
}

/*---------------------------------------------------------------------------*/
int FrvFDFilterAddPole(FrvFDFilter *fdf, double freq, double q)
/*---------------------------------------------------------------------------*/
{double b, b1, b2;
 int ierr;

 b = 1./(FRTWOPI*freq);
 if(q == 0)
      {b1 = b;
       b2 = 0;}
 else {b1 = b/q;
       b2 = b*b;}

 ierr = FrvFDFilterAddFilter2(fdf, 0, 1., 0., 0., 0., 1., b2, b1, 1.);

 return(ierr);
}

/*---------------------------------------------------------------------------*/
FrvFDFTFBin* FrvFDFilterAddTFBin(FrvFDFilter *fdf,
			double freq, double scale, double phase)
/*---------------------------------------------------------------------------*/
{FrvFDFTFBin *tfBin;

 if(fdf == NULL) return(NULL);

 tfBin = (FrvFDFTFBin *) calloc(1,sizeof(FrvFDFTFBin));
 if(tfBin == NULL) return(NULL);

 if(   fdf->tfBinLast == NULL) {fdf->tfBins = tfBin;}
 else {fdf->tfBinLast->next = tfBin;}
 fdf->tfBinLast = tfBin;

 tfBin->freq  = freq;
 tfBin->scale = scale;
 tfBin->phase = phase;

 return(tfBin);
}
/*---------------------------------------------------------------------------*/
int FrvFDFilterAddZero(FrvFDFilter *fdf, double freq, double q)
/*---------------------------------------------------------------------------*/
{double a, a1, a2;
 int ierr;

 a = 1./(FRTWOPI*freq);
 if(q == 0)
      {a1 = a;
       a2 = 0;}
 else {a1 = a/q;
       a2 = a*a;}

 ierr = FrvFDFilterAddFilter2(fdf, 0, 1., 0., a2, a1, 1, 0., 0., 1.);

 return(ierr);
}

/*---------------------------------------------------------------------------*/
void* FrvFDFilterFree(FrvFDFilter *fdf)
/*---------------------------------------------------------------------------*/
{FrvFDFHole *hole, *nextHole;
 FrvFDFilter2 *filter2, *nextFilter2;
 FrvFDFTFBin *tfBin, *nextTfBin;

 if(fdf->buffer != NULL) FrvBufFree(fdf->buffer);

 FrVectFree(fdf->fft);
 FrVectFree(fdf->tf);
 FrVectFree(fdf->tfModulus);
 FrVectFree(fdf->tfPhase);
 FrVectFree(fdf->fftInput);

 if(fdf->plan_for  != NULL) fftw_destroy_plan(fdf->plan_for);

 for(hole=fdf->hole; hole != NULL; hole = nextHole)
    {nextHole = hole->next;
     free(hole);}

 for(filter2=fdf->filter2; filter2 != NULL; filter2 = nextFilter2)
    {nextFilter2 = filter2->next;
     free(filter2);}

 for(tfBin = fdf->tfBins; tfBin != NULL; tfBin = nextTfBin)
    {nextTfBin = tfBin->next;
     free(tfBin);}

 free(fdf);

 return(NULL);
}

/*---------------------------------------------------------------------------*/
int FrvFDFilterBuildTF(
  FrvFDFilter *fdf,   /* pointer to a frequency domain filter structure      */
  char *prefix)       /* prefix for the TF names                             */
/*---------------------------------------------------------------------------*/
/* This function build the filter transfert function                         */
{int i, nReal;
  double dnu, freq, phase, omega, omega2, inv, dTFR, dTFI, sigma;
 double numR, denR, numI, denI, tfR, tfRold, tfI, termR, termI;
 FrvFDFHole *hole;
 FrvFDFilter2 *filter2;
 FrvFDFPhaseBump *phaseB;
 char *name;

 if(fdf->fft == NULL) return(-4);
 fdf->tf = FrVectCopyToD(fdf->fft, 0, "TF");
 if(fdf->tf == NULL) return(-3);

 dnu   = fdf->tf->dx[0];
 nReal = fdf->tf->nData/2+1;

 name = malloc(strlen(prefix) + 30);
 if(name == NULL) return(-4);
 sprintf(name,"%s_TF_modulus", prefix);
 fdf->tfModulus = FrVectNew1D(name,-FR_VECT_8R, nReal, dnu,"Hz","TF modulus");
 sprintf(name,"%s_TF_phase", prefix);
 fdf->tfPhase = FrVectNew1D(name,-FR_VECT_8R, nReal, dnu,"Hz","TF modulus");
 if(fdf->tfModulus == NULL || 
    fdf->tfPhase   == NULL) return(-2);
 free(name);

       /*---------------------------- build the TF using a set of values ----*/

 if(fdf->tfBins != NULL)
   {return(FrvFDFilterBuildTFBin(fdf));}

       /*------------- loop on all frequencies (the first and last are real) */

 for(i=0; i<nReal; i++)
   {freq = i*dnu;
                                      /*-------- First just the delay -------*/

    phase = fdf->delay * freq * FRTWOPI;
    tfR = fdf->gain*cos(-phase);
    tfI = fdf->gain*sin(-phase);
                                     /*----------- apply first the hole -----*/

    for(hole = fdf->hole; hole != NULL; hole = hole->next)
      {if(freq <= hole->fMin) continue;
       if(freq >= hole->fMax) continue;
       phase = hole->delay * freq * FRTWOPI;
       termR = hole->scale * cos(phase);
       termI = hole->scale * sin(phase);
       tfRold = tfR;
       tfR = tfRold * termR - tfI * termI;
       tfI = tfRold * termI + tfI * termR;}
                                    /*--- apply the second order filter -----*/
    omega  = freq * FRTWOPI;
    omega2 = omega * omega;
    for(filter2 = fdf->filter2; filter2 != NULL; filter2 = filter2->next)
      {if(freq <= filter2->fStart)
	 {phase = filter2->delay * freq * FRTWOPI;
          termR = filter2->scale * cos(-phase);
          termI = filter2->scale * sin(-phase);}
       else
	 {numR = filter2->a0 - filter2->a2 * omega2;
          numI = filter2->a1 * omega;
          denR = filter2->b0 - filter2->b2 * omega2;
          denI = filter2->b1 * omega;
          inv = 1./(denR*denR + denI*denI);
          termR = (numR * denR + numI * denI) * inv;
          termI = (numI * denR - numR * denI) * inv;}
       tfRold = tfR;
       tfR = tfRold * termR - tfI * termI;
       tfI = tfRold * termI + tfI * termR;}

    /*------ add the phase bump -----*/

    for(phaseB = fdf->phaseBump; phaseB != NULL; phaseB = phaseB->next) {
      sigma = (freq - phaseB->freq) / phaseB->width;
      phase = phaseB->dPhi * exp(-sigma*sigma);
      termR = cos(phase);
      termI = sin(phase);
      tfRold = tfR;
      tfR = tfRold * termR - tfI * termI;
      tfI = tfRold * termI + tfI * termR;}

                   /*----- add TF variation if additional vector is there ---*/

    if(fdf->dTF != NULL) {
      if(i < fdf->dTF->nData/2) {
	dTFR =  fdf->dTF->dataD[i];
	dTFI =  fdf->dTF->dataD[fdf->dTF->nData-i];
	tfR = tfR*(1.+dTFR) - tfI*   dTFI;
	tfI = tfR*    dTFI  + tfI*(1+dTFR);}}

                                    /*-----fill the TF, modulus and phase ---*/
    fdf->tf->dataD[i] = tfR;
    fdf->tfModulus->dataD[i] = sqrt(tfR * tfR + tfI * tfI);
    if(i == 0 || i == nReal-1) continue;
    fdf->tf->dataD[fdf->tf->nData - i] = tfI;
    fdf->tfPhase->dataD[i]  = atan2(tfI,tfR);}
  
 return(0);}
/*---------------------------------------------------------------------------*/
int FrvFDFilterBuildTFBin(FrvFDFilter *fdf)
/*---------------------------------------------------------------------------*/
/* This function build the filter transfert function using a set of points   */
/*---------------------------------------------------------------------------*/
{int i, nReal;
 double dnu, scale, scale0, scale1, freq, freq0, freq1, phase, phase0, phase1;
 FrvFDFTFBin *tfBin;

 dnu   = fdf->tf->dx[0];
 nReal = fdf->tf->nData/2+1;

       /*------------- loop on all frequencies (the first and last are real) */

 tfBin  = fdf->tfBins;
 freq1  = tfBin->freq;
 scale1 = tfBin->scale;
 phase1 = 0;     /* the first bin is just a rescalling --*/

 freq0  = 0.;
 scale0 = scale1;
 phase0 = 0;

 for(i=0; i<nReal; i++)
   {freq = i*dnu;
                                      /*-------- should we change bin-------*/
     while(freq > freq1 && tfBin->next != NULL)
       {freq0 = freq1;
	 scale0 = scale1;
	 phase0 = phase1;
	 tfBin = tfBin->next;
	 freq1  = tfBin->freq;
	 scale1 = tfBin->scale;
	 phase1 = tfBin->phase;}

    if(freq1-freq > 0) {
       scale = scale0 + (freq-freq0) * (scale1-scale0)/(freq1-freq0);
       phase = phase0 + (freq-freq0) * (phase1-phase0)/(freq1-freq0);}
    else {
       scale = scale0;
       phase = phase0;}

                                    /*-----fill the TF, modulus and phase ---*/
    fdf->tf->dataD[i] = scale * cos(phase);
    fdf->tfModulus->dataD[i] = scale;
    if(i == 0 || i == nReal-1) continue;
    fdf->tf->dataD[fdf->tf->nData - i] = scale * sin(phase);
    fdf->tfPhase->dataD[i]  = phase;}
  
 return(0);}

/*---------------------------------------------------------------------------*/
int FrvFDFilterInit(
  FrvFDFilter *fdf,   /* pointer to a frequency domain filter structure      */
  FrVect *data)       /* vector of data to be processed                      */
/*---------------------------------------------------------------------------*/
{int i, nSkip;
 double a, b, c, dnu;

          /*---- this code works only if the input nData is an even number---*/
 if(data->nData%2 != 0) return(-9);

  /*--- create buffer: The fft is done on twice the input vector size
  There is a 50% overlapp since only the central part of the output is used-*/

 fdf->buffer = FrvBufNewID(2*data->nData, data->nData, FR_VECT_8R, 0, 0);
 if(fdf->buffer == NULL) return(1);

           /*--create vector to store input data to the FFT (data * window) */

 fdf->fftInput = FrVectNew1D("fft_input(input*window)",-FR_VECT_8R, 
                       2*data->nData, data->dx[0], data->unitX[0], NULL);

             /*-------------------------------- create the input window -----*/

 fdf->windowIn = FrvClone(fdf->fftInput,"tapering_window");
 if(fdf->windowIn == NULL) return(-1);

 nSkip = SKIP*((double)fdf->windowIn->nData);
 a = 2.*nSkip;
 b = FRTWOPI/a;
 c = .5/data->nData;
 for(i=nSkip; i<fdf->windowIn->nData-nSkip; i++)
   {fdf->windowIn->dataD[i] = c;}

 for(i=0; i<nSkip; i++)
   {fdf->windowIn->dataD[i] = c*0.5*(1. - cos(b*(i+.5)));
    fdf->windowIn->dataD[fdf->windowIn->nData-i-1] = fdf->windowIn->dataD[i];}

          /*----------------- create buffer for the fft ---------------------*/

 if(data->dx[0] == 0) 
      dnu = 1.;
 else dnu = 1./(data->dx[0]*2.*data->nData);

 fdf->fft   = FrVectNew1D("FFT",-FR_VECT_8R, fdf->fftInput->nData, 
                                      dnu, "freq [Hz]","A.U.");
 fdf->fftTF = FrVectCopyToD(fdf->fft, 0, "FFT_time_TF");
 if(fdf->fftTF == NULL) return(-2);

         /*------------------------------ create the transfer function ------*/

 FrvFDFilterBuildTF(fdf, data->name);

            /*--------------- create FFTW plan ------------------------------*/
 
 fdf->plan_for = fftw_plan_r2r_1d(fdf->fftInput->nData,
                                  fdf->fftInput->dataD,
                                  fdf->fft->dataD, FFTW_R2HC,  FFTW_ESTIMATE);
 /***** to be more efficient at the price of a slow init use   FFTW_MEASURE **/

 if(fdf->plan_for  == NULL) return(-3);

 fdf->nData = data->nData;

 /*------ init the output part ------*/

 FrvFDFilterOutInit(fdf->fOut, fdf->fftTF);

 return(0);}
/*---------------------------------------------------------------------------*/
int FrvFDFilterOutInit(FrvFDFilterOut *fOut, FrVect* fftTF)
/*---------------------------------------------------------------------------*/
/* Init the object doing the reverse FFT to the tome domain                  */
{
  int i, nSkip, nDataOut, n2;
  double a, b, x, dx, lOut, *wOut;

             /*-------------------------create resized FFT if needed ---*/

 if(fOut->sampleRateOut > 0 )
   {lOut = 1./fftTF->dx[0]/2.;   /*-- this is the duration of the fft */ 
    dx = 1./fOut->sampleRateOut;
    nDataOut = (lOut+.5*dx)/dx;}
 else
   {dx = 1./fftTF->dx[0]/fftTF->nData;
    nDataOut = fftTF->nData/2;}

 fOut->fftResized = FrVectNew1D("FFT",-FR_VECT_8R, 2*nDataOut, 
                                 fftTF->dx[0],"freq [Hz]","A.U.");
 if(fOut->fftResized == NULL) return(-2);

             /*-------------------------------- create the output window ----*/

 fOut->windowOut = FrVectNew1D("windowOut",-FR_VECT_8R,  nDataOut, 
                                             dx, "s", NULL);
 if(fOut->windowOut == NULL) return(-1);
 wOut = fOut->windowOut->dataD;

 if(SKIP == .5) {
   nSkip = fOut->deadZone*((double)nDataOut);
   a = FRPI/nDataOut;
   b = FRPI/nSkip;
   n2 = nDataOut-nSkip;
   for(i=0;  i<nSkip; i++) {wOut[i] = .5*(1. - cos(b*(i+.5)));}
   for(i=nSkip; i<n2; i++) {wOut[i] = 1.;}

   for(i=n2; i < nDataOut; i++) {
     x = i+0.5;
     wOut[i] = (2.-0.5*(1.-cos(b*x))*(1+cos(a*x)))/(1.-cos(a*x));}}
 else {
   nSkip = SKIP*((double)nDataOut);
   a = 2.*(nDataOut-2*nSkip) + 1.;
   b = FRTWOPI/a;
   for(i=0; i<nSkip; i++)                 {wOut[i] = 0.;}
   for(i=nSkip; i<nDataOut-nSkip; i++)    {wOut[i] = .5*(1.-cos(b*((i-nSkip+.5))));}
   for(i=nDataOut-nSkip; i<nDataOut; i++) {wOut[i] = 1.;}}

           /*------------------------- create output vectors ----------------*/

 fOut->fftOut = FrVectNew1D("fft_output",-FR_VECT_8R, 2*nDataOut, 
			             dx, "s", NULL);
 fOut->output = FrVectCopyToD(fOut->windowOut,0, "filter_output");
 fOut->last   = FrVectCopyToD(fOut->output,   0, "lastVector");
 if(fOut->last == NULL) return(-2);

            /*--------------- create FFTW plan ------------------------------*/
 
 /****** to be more efficient but slow init use  FFTW_MEASURE ***/

 fOut->plan_back = fftw_plan_r2r_1d(fOut->fftOut->nData,
                                   fOut->fftResized->dataD,
				   fOut->fftOut->dataD,FFTW_HC2R, FFTW_ESTIMATE);
 if(fOut->plan_back == NULL) return(-3);

 return(0);}
/*---------------------------------------------------------------------------*/
FrvFDFilterOut* FrvFDFilterOutNew(double sampleRate)
/*---------------------------------------------------------------------------*/
{FrvFDFilterOut *fOut;

 fOut = FrvFDFilterOutNewHP(sampleRate, 0.);

 return(fOut);}

/*---------------------------------------------------------------------------*/
FrvFDFilterOut* FrvFDFilterOutNewHP(double sampleRate, double fStart)
/*---------------------------------------------------------------------------*/
{FrvFDFilterOut *fOut;

 fOut = (FrvFDFilterOut*) calloc(1, sizeof(FrvFDFilterOut));
 if(fOut == NULL) return(NULL);
 fOut->sampleRateOut = sampleRate;
 fOut->deadZone  = .25;
 fOut->freqStart = fStart;

 return(fOut);}

/*---------------------------------------------------------------------------*/
void FrvFDFilterOutProc(FrvFDFilterOut *fOut, FrVect* fftTF, double scale)
/*---------------------------------------------------------------------------*/
{int i, nData, nReal, iImag, offset, nResized, nStart, nEnd;
  double *windowOut, *fftTFD, *last, *output, *fftResized, *fftOut, sum;
 double dt, lastEnd;

 if(fOut ==  NULL) return;

 if(fOut->fftResized == NULL)  FrvFDFilterOutInit(fOut, fftTF);

      /*--copy the data from the input vector fftTF to the internal 
          vector fftResized, changing the number of bin if requested;            
            reminder: if nData = 3  (6 input data) 
            the FFT data are store like r0,r1,r2,r3,i2,i1 ---*/ 

 nResized =  fOut->fftResized->nData;
     
 if(nResized > fftTF->nData) {           /*------increase the number of bins--*/
   nReal  = fftTF->nData/2 + 1;}
 else {                                  /*keep or reduce the number of bins--*/
   nReal  = nResized/2 + 1;}

 nStart = fOut->freqStart/fftTF->dx[0]; /*-freq cut for the high pass filter--*/
 if(nStart > nReal) nStart = nReal;
 nEnd = nResized - nStart;

 fftTFD     = fftTF->dataD;
 fftResized = fOut->fftResized->dataD;
 iImag  = nResized - nReal + 2;
 offset = fftTF->nData - nResized;

 for(i=1;     i<nStart;   i++) {fftResized[i] = 0.;}
 for(i=nStart;i<nReal;    i++) {fftResized[i] = scale*fftTFD[i];}
 for(i=nReal; i<iImag;    i++) {fftResized[i] = 0.;}
 for(i=iImag; i<nEnd;     i++) {fftResized[i] = scale*fftTFD[offset+i];}
 for(i=nEnd;  i<nResized; i++) {fftResized[i] = 0.;}
fftResized[0] = scale*fftTFD[0];
                                    /*--------------------------invert FFT --*/

 fftw_execute((fftw_plan) fOut->plan_back);

                                    /*- normalise the output to a zero mean--*/
 nData  = fOut->fftOut->nData;
 fftOut = fOut->fftOut->dataD;

 sum = 0;
 for(i=0; i<nData; i++) {sum += fftOut[i];}
 sum = sum/nData;
 for(i=0; i<nData; i++) {fftOut[i] -= sum;}

                                    /*---------------------- prepare output--*/
 nData     = fOut->output->nData;
 windowOut = fOut->windowOut->dataD;
 last      = fOut->last->dataD;
 output    = fOut->output->dataD;

                       /*-- reset previous output in case of discontinuity --*/
 fOut->output->GTime = fftTF->GTime;
 lastEnd = fOut->last->GTime + nData * fOut->last->dx[0];
 dt = fOut->output->GTime - lastEnd;
 if(dt > fOut->last->dx[0]) {
   for(i=0; i<nData; i++) {last[i] = 0;}}

                                    /*---merge previous and current output---*/
 if(SKIP == .5) {
   for(i=0; i<nData; i++) {
     output[i] = fftOut[i] * windowOut[i] + last[i] * windowOut[nData - i -1];}}
 else {
   for(i=0; i<nData; i++) {
     output[i] = fftOut[i] * windowOut[i] + last[i] * (1.-windowOut[i]);}}

 for(i=0; i<nData; i++) {last[i] = fftOut[i+nData];}
 fOut->last->GTime = fOut->output->GTime;
    
 return;}

/*---------------------------------------------------------------------------*/
FrvFDFilter *FrvFDFilterNew(
   double freqMax,
   double gain,
   double delay,
   double sampleRateOut)
/*---------------------------------------------------------------------------*/
{FrvFDFilter *fdf;

 fdf = (FrvFDFilter *) calloc(1,sizeof(FrvFDFilter));
 if(fdf == NULL) return(NULL);

 fdf->gain  = gain;
 fdf->delay = delay;
 fdf->first = 0;
 fdf->hole    = NULL;
 fdf->filter2 = NULL;
 fdf->next    = NULL;
 FrvFDFilterAddHole(fdf, freqMax, 1.e20, 0., 0.);

 fdf->fOut = FrvFDFilterOutNew(sampleRateOut);

 return(fdf);}

/*---------------------------------------------------------------------------*/
double FrvFDFilterPhaseAt(
  FrvFDFilter *fdf,   /* pointer to a frequency domain filter structure      */
  double freq)
/*---------------------------------------------------------------------------*/
/* This function return the phase for a frequency                            */
{
  double phase, omega, omega2, inv;
  double numR, denR, numI, denI, tfR, tfRold, tfI, termR, termI;
  FrvFDFilter2 *filter2;

  phase = fdf->delay * freq * FRTWOPI;
  tfR = cos(phase);
  tfI = sin(phase);
  /*--- apply the second order filter -----*/
  omega  = freq * FRTWOPI;
  omega2 = omega * omega;
  for(filter2 = fdf->filter2; filter2 != NULL; filter2 = filter2->next) {
    numR = filter2->a0 - filter2->a2 * omega2;
    numI = filter2->a1 * omega;
    denR = filter2->b0 - filter2->b2 * omega2;
    denI = filter2->b1 * omega;
    inv = 1./(denR*denR + denI*denI);
    termR = (numR * denR + numI * denI) * inv;
    termI = (numI * denR - numR * denI) * inv;
    tfRold = tfR;
    tfR = tfRold * termR - tfI * termI;
    tfI = tfRold * termI + tfI * termR;}

  phase  = atan2(tfI,tfR);

return(phase);}
/*---------------------------------------------------------------------------*/
int FrvFDFilterProcFFT( 
 FrvFDFilter *fdf,    
 FrVect *vect) 
/*---------------------------------------------------------------------------*/
{
 if(fdf ==  NULL) return(-1);
 if(vect == NULL) return(-2);

 if(fdf->nData == 0) if(FrvFDFilterInit(fdf, vect) != 0) return(-3);
 if(fdf->nData != vect->nData) return(-6);
                                    /*----feed and extract the input buffer--*/
 if(FrvBufFeed(fdf->buffer,vect) != 0) return(-4);
 if(FrvBufGetNext(fdf->buffer)   != 0) return(-5);

                                    /*apply tapering window: buffer->fftInput*/
 FrvMult(fdf->buffer->output, fdf->windowIn, fdf->fftInput, NULL);
 
                                   /*----------- do the FFT; fftInput->fft--*/
 fftw_execute((fftw_plan) fdf->plan_for);
 fdf->fft->GTime = fdf->buffer->output->GTime;

 return(0);}
/*---------------------------------------------------------------------------*/
int FrvFDFilterProc( 
 FrvFDFilter *fdf,    
 FrVect *vect) 
/*---------------------------------------------------------------------------*/
{int irc;

 if(fdf ==  NULL) return(-1);
 if(vect == NULL) return(-2);

                               /*-get data and go to the frequency domain ---*/
 irc = FrvFDFilterProcFFT(fdf, vect);
 if(irc != 0) return(irc);

                                    /*---apply the TF in the FFTW order----- */
 FrvFDFilterProcTF(fdf);

                                    /*------------go back to the time time --*/
 FrvFDFilterOutProc(fdf->fOut, fdf->fftTF, 1.);

 return(0);}
/*---------------------------------------------------------------------------*/
int FrvFDFilterProcTF(FrvFDFilter *fdf) 
/*---------------------------------------------------------------------------*/
{int i, nData, last;
  double rTF, iTF, rFFT, iFFT, *fft, *tf, *fftTF;

 if(fdf ==  NULL) return(-1);
                              /*--apply the TF in the FFTW order: fft->fftTF */
 nData = fdf->fft->nData;
 last  = nData/2+2;
 fft   = fdf->fft->dataD;
 tf    = fdf->tf->dataD;
 fftTF = fdf->fftTF->dataD;
     
 for (i=1; i<last; i++) 
    {rFFT = fft[i];
     iFFT = fft[nData-i];
     rTF  = tf[i];
     iTF  = tf[nData-i];
     fftTF[i]       = rFFT*rTF - iFFT*iTF;
     fftTF[nData-i] = rFFT*iTF + iFFT*rTF;}
 
 fftTF[0]    = fft[0]    * tf[0];
 fftTF[last] = fft[last] * tf[last];

 fdf->fftTF->GTime = fdf->fft->GTime;

 return(0);}

/*---------------------------------------------------------------------------*/
void FrvFDFilterSetName(FrvFDFilter *fdf, 
                       char *name)
/*---------------------------------------------------------------------------*/
{
 if(fdf == NULL) return;

 FrStrCpy(&(fdf->name), name);

 return;
}
/*---------------------------------------------------------------------------*/
int FrvFDFilterSetSkip(FrvFDFilter *fdf, double skip)
/*---------------------------------------------------------------------------*/
{
 if(fdf == NULL) return(-1); 

 if(skip <   0) skip = 0;
 if(skip > .25) skip = .25;

 SKIP = skip;

 return(0);}
